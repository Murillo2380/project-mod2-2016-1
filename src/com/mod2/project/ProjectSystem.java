package com.mod2.project;

import com.mod2.project.authenticatorutils.login.AbstractSectionLoginManager;
import com.mod2.project.authenticatorutils.login.interfaces.SectionActionListener;
import com.mod2.project.authenticatorutils.login.interfaces.action.SectionAction;
import com.mod2.project.db.ModProjectDatabase;
import com.mod2.project.ui.LoginPrivilegedUI;
import com.sun.istack.internal.NotNull;

/**
 * Classe que inicia o sistema de sócios.
 */
public class ProjectSystem implements SectionActionListener {

    public static final int GENERAL_WINDOW_WIDTH = 400;
    public static final int GENERAL_WINDOW_HEIGHT = 300;

    private AbstractSectionLoginManager.Section section;

    /**
     * Inicia a classe realizando a connexão com o banco de dados.
     */
    public ProjectSystem(@NotNull String dbDriver, @NotNull String dbUrl, @NotNull String user, @NotNull String password){

        ModProjectDatabase.getInstance().connectDB(dbDriver, dbUrl, user, password);
        start();

    }

    private void start(){

        LoginPrivilegedUI loginUI = new LoginPrivilegedUI(this, GENERAL_WINDOW_WIDTH, GENERAL_WINDOW_HEIGHT);
        loginUI.setVisible(true);

    }

    //public void close(){
     //   ModProjectDatabase.getInstance().disconnectDB();
    //}

    public void userConnected(AbstractSectionLoginManager.Section section){
        this.section = section;
        section.addSectionLogoutListener(this);

    }

    @Override
    public int onSectionAction(AbstractSectionLoginManager.Section section, SectionAction action) {

        if(action.getAction() == SectionAction.ACTION_LOGOUT && section == this.section){

            this.section = null;
            start(); // reopen the default loginUI
            return AbstractSectionLoginManager.Section.REQUEST_REMOVE_LISTENER;
        }

        return AbstractSectionLoginManager.Section.REQUEST_NOTHING;

    }

}
