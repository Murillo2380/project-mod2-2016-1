package com.mod2.project.authenticatorutils;

import com.sun.istack.internal.NotNull;

/**
 * Classe com definições de nome e privilégios de usuário.
 */
public class User {

    /**
     * Para privilégios de super usuário.
     */
    public static final int PRIVILEGE_ADMIN = 100;

    /**
     * Para privilégios normais.
     */
    public static final int PRIVILEGE_NORMAL = 50;

    /**
     *  Para privilégio baixo.
     */
    public static final int PRIVILEGE_LOW = 25;

    /**
     * Para privilégio de clientes.
     */
    public static final int PRIVILEGE_CLIENT = 5;

    /**
     * Nenhum privilégio.
     */
    public static final int PRIVILEGE_ANY = 1;

    /**
     * Privilégio inválido.
     */
    public static final int PRIVILEGE_INVALID = 0;

    /**
     * Privilégio deste usuário
     * @see #PRIVILEGE_ADMIN
     * @see #PRIVILEGE_CLIENT
     * @see #PRIVILEGE_INVALID
     * @see #PRIVILEGE_LOW
     * @see #PRIVILEGE_NORMAL
     */
    private int privilege;

    /**
     * Nome deste usuário.
     */
    private String userName;

    public User(@NotNull String userName, @NotNull int privilege){
        this.userName = userName;
        this.privilege = privilege;
    }

    /**
     * @return Nome do usuário.
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @return Privilégio deste usuário.
     * @see #PRIVILEGE_ADMIN
     * @see #PRIVILEGE_CLIENT
     * @see #PRIVILEGE_INVALID
     * @see #PRIVILEGE_LOW
     * @see #PRIVILEGE_NORMAL
     * @see #PRIVILEGE_ANY
     */
    public int getPrivilege() {
        return privilege;
    }

}
