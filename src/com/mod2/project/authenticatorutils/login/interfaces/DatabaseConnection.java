package com.mod2.project.authenticatorutils.login.interfaces;

import java.sql.Connection;

/**
 * Created by Murillo on 04/04/2016.
 */
public interface DatabaseConnection {

    /**
     * Configura a conexão de dados sql.
     * @param sqlDriver Driver sql. Para o postgres por exemplo, quando a biblioteca já está instalada, seria "org.postgresql.Driver"
     * @param url Url para o banco de dados.
     * @param user Usuário do banco de dados.
     * @param password senha do banco de dados.
     */
    Connection connectDB(String sqlDriver, String url, String user, String password);

    /**
     * Desconnecta do banco de dados sql.
     */
    void disconnectDB();

    /**
     * @return True se a conexão for estabelecida com sucesso.
     */
    boolean isConnected();

}
