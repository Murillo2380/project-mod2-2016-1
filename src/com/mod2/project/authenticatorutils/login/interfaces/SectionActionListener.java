package com.mod2.project.authenticatorutils.login.interfaces;

import com.mod2.project.authenticatorutils.login.AbstractSectionLoginManager;
import com.mod2.project.authenticatorutils.login.interfaces.action.SectionAction;

/**
 * Escutador para possíveis ações de sessão.
 */
public interface SectionActionListener {

    /**
     * Chamado sempre que houver uma ação prioritária da sessão.
     * @param section Sessão que gerou a ação.
     * @param action Classe contendo a flag que gerou esta chamada
     * @return Código que pode ser usado por alguma implementação
     */
    int onSectionAction(AbstractSectionLoginManager.Section section, SectionAction action);
}
