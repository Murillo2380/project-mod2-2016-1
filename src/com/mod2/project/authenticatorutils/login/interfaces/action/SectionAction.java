package com.mod2.project.authenticatorutils.login.interfaces.action;

import com.mod2.project.authenticatorutils.login.AbstractSectionLoginManager;
import com.mod2.project.authenticatorutils.login.interfaces.SectionActionListener;

/**
 * <p>Classe de ação que é usada em {@link com.mod2.project.authenticatorutils.login.interfaces.SectionActionListener}.</p>
 * <p>Esta classe age como uma flag, que pode verificar qual ação disparou o escutador {@link com.mod2.project.authenticatorutils.login.interfaces.SectionActionListener#onSectionAction(AbstractSectionLoginManager.Section, SectionAction)}</p>
 * @see #getAction()
 */
public class SectionAction {

    /**
     * Código lançado quando a sessão é desconectada através do método {@link AbstractSectionLoginManager.Section#logout() }.
     * @see AbstractSectionLoginManager.Section#isSectionExpired()
     */
    public static final int ACTION_LOGOUT = 1;


    /**
     * Ação gerada
     */
    private int action;

    /**
     *
     * @param action Ação gerada para esta classe.
     * @see #ACTION_LOGOUT
     */
    public SectionAction(int action){
        this.action = action;
    }

    /**
     *
     * @return Ação gerada para alguma sessão.
     * @see #ACTION_LOGOUT
     */
    public int getAction(){
        return action;
    }

}
