package com.mod2.project.authenticatorutils.login.interfaces;

import com.mod2.project.authenticatorutils.exception.AbstractLoginException;
import com.mod2.project.db.ModProjectDatabase;
import com.sun.istack.internal.Nullable;

/**
 * Interface para implementação de autenticação da aplicação, podendo variar dependendo do sistema.
 */
public interface Authenticator {

    /**
     * Tenta realizar uma autenticação
     * @param user Usuário para autenticação.
     * @param password senha
     * @return Sessão do login se encontrar um usuário com o nome e senha, null caso contrário.
     * @throws AbstractLoginException Se houver algum erro na tentativa de login.
     */
    @Nullable
    ModProjectDatabase.Section attemptLogin(String user, String password) throws AbstractLoginException;


}
