package com.mod2.project.authenticatorutils.login.interfaces;

import com.mod2.project.authenticatorutils.login.AbstractSectionLoginManager;

/**
 * Escutador de logins, de maneira a saber se um login foi bem ou mal sucedido.
 */
public interface AuthenticatorListener {

    /**
     * Chamado quando um usuário for autenticado com sucesso.
     * @param section Sessão autenticado.
     * @return Código que pode ser usado para alguma finalidade, dependendo da implementação.
     */
    int onLoginAttemptSuccess(AbstractSectionLoginManager.Section section);

    /**
     * Chamado quando o usuário falhou em ser autenticado.
     * @param user Usuário utilizado
     * @param password Senha utilizado
     * @return Código que pode ser usado para alguma finalidade, dependendo da implementação.
     */
    int onLoginAttemptFailed(String user, String password);


}
