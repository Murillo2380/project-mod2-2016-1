package com.mod2.project.authenticatorutils.login;

import com.mod2.project.authenticatorutils.User;
import com.mod2.project.authenticatorutils.login.interfaces.Authenticator;
import com.mod2.project.authenticatorutils.login.interfaces.SectionActionListener;
import com.mod2.project.authenticatorutils.login.interfaces.action.SectionAction;
import com.mod2.project.authenticatorutils.ui.AbstractPrivilegedUI;
import com.mod2.project.db.ModProjectDatabase;
import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * Classe abstrata que deve ser implementada, a fim de ser possível realizar uma autenticação
 * e retornar uma sessão, deste modo é possível criar uma janela privilegiada.
 * @see ModProjectDatabase
 * @see AbstractPrivilegedUI
 * @see Section
 * @see Authenticator
 */
public abstract class AbstractSectionLoginManager implements Authenticator {

    /**
     * @param user Usuário que iniciou a sessão
     * @return Sessão do usuário autenticado.
     */
    protected Section newSection(User user){
        return new Section(user);
    }

    /**
     * <p>Através de sessões você pode chegar ao usuário conectado, esta classe
     * deve ser usada a fim de criar um login válido usando o método. É importante
     * verificar se a sessão não expirou, através do método</p>
     * {@link #isSectionExpired()}.
     * <p>É possível saber quando houve o logout através do escutador {@link SectionActionListener}, usando
     * {@link #addSectionLogoutListener(SectionActionListener)}</p>
     */
    public class Section{

        /**
         * Ao invés de chamar a função {@link #removeSectionLogoutListener(SectionActionListener)} dentro da implementação
         * da função {@link SectionActionListener#onSectionAction(Section, SectionAction)},
         * use este valor como retorno para estes último método,
         * de modo a remover o escutador de dentro da implementação de maneira segura.
         */
        public static final int REQUEST_REMOVE_LISTENER = 171;
        /**
         * Se usar este valor como retorno do método {@link SectionActionListener#onSectionAction(Section, SectionAction)},
         * nenhuma operação em especial é realizada.
         * @see #REQUEST_REMOVE_LISTENER
         */
        public static final int REQUEST_NOTHING = 0;

        /**
         * Lista de ecutador para {@link SectionActionListener}
         */
        private LinkedList<SectionActionListener> listenerList;

        /**
         * Usuário da sessão.
         */
        private User connectedUser;

        /**
         * Pode apenas ser criada via {@link #attemptLogin(String, String)}
         */
        private Section(User user){
            this.connectedUser = user;
            listenerList = new LinkedList<>();
        }

        /**
         * Faz com que {@link #isSectionExpired()} retorne false. Ao chamar esta função, esta instância deverá
         * ser descartada pois não haverá mais utilidade na autenticação.
         * @see SectionAction
         */
        public void logout(){

            connectedUser = null;
            Iterator<SectionActionListener> iterator = listenerList.iterator();

            while(iterator.hasNext()) {
                if(iterator.next().onSectionAction(Section.this, new SectionAction(SectionAction.ACTION_LOGOUT)) == REQUEST_REMOVE_LISTENER)
                    iterator.remove();
            }

        }

        /**
         *
         * @return Verdadeiro se a sessão é válida.
         * @see #logout()
         */
        public boolean isSectionExpired(){
            return connectedUser == null;
        }

        /**
         * Adiciona um escutador para o método {@link #logout()}. As chamadas deste
         * escutador são assíncronas.
         * @param listener Instância da classe.
         * @see #removeSectionLogoutListener(SectionActionListener)
         */
        public void addSectionLogoutListener(@NotNull SectionActionListener listener){
            listenerList.add(listener);
        }

        /**
         * Remove uma instância do escutador para o método {@link #logout()}
         * @param listener Instância para ser removida, caso haja na lista.
         * @see #addSectionLogoutListener(SectionActionListener)
         */
        public void removeSectionLogoutListener(@NotNull SectionActionListener listener){
            listenerList.remove(listener);
        }

        /**
         * @return Usuário desta sessão. Retorna null caso a sessão
         * tenha expirado.
         * @see #isSectionExpired()
         * @see #logout()
         */
        public @Nullable User getConnectedUser() {
            return connectedUser;
        }

    }


}
