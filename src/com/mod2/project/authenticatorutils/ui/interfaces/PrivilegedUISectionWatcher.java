package com.mod2.project.authenticatorutils.ui.interfaces;

import com.mod2.project.authenticatorutils.login.AbstractSectionLoginManager;
import com.mod2.project.authenticatorutils.ui.AbstractPrivilegedUI;

/**
 * Usado para registrar falhas de sessão ocorridas ao chamar o método {@link AbstractPrivilegedUI#setVisible(boolean)}
 */
public interface PrivilegedUISectionWatcher {

    /**
     * Chamado quando a sessão da janela expirou. A janela está para ser fechada após esta chamada.
     * @param ui Janela que detectou a sessão expirada.
     * @param section Sessão expirada.
     */
    void onSectionExpiredDetected(AbstractPrivilegedUI ui, AbstractSectionLoginManager.Section section);


    /**
     * Chamada quando não foi passada uma sessão para a janela. Deve ser chamada
     * {@link AbstractPrivilegedUI#setCurrentSection(AbstractSectionLoginManager.Section)}
     * @param ui Janela sem sessão.
     */
    void onSectionNotRegisteredDetected(AbstractPrivilegedUI ui);


    /**
     * Chamada quando o privilégio da sessão não é suficiente para utilizar a janela.
     * A janela está para ser fechada após a chamada desta função.
     * @param ui Janela que impediu o acesso.
     * @param section Sessão sem privilégio.
     */
    void onSectionNotAllowedDetected(AbstractPrivilegedUI ui, AbstractSectionLoginManager.Section section);

}
