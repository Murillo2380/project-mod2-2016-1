package com.mod2.project.authenticatorutils.ui;

import com.mod2.project.authenticatorutils.User;
import com.mod2.project.authenticatorutils.ui.interfaces.PrivilegedUISectionWatcher;
import com.mod2.project.authenticatorutils.login.AbstractSectionLoginManager;
import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.LinkedList;

/**
 * Classe abstrata para todas as janelas deste programa, implementando autenticação de usuário.
 * Por padrão, {@link #setLayout(LayoutManager)} é definido com null.
 */
public abstract class AbstractPrivilegedUI extends JFrame{

    /**
     * Argumentos desta classe.
     */
    private Object args = null;

    /**
     * Mínimo privilégio para esta classe.
     */
    private int minAccessPrivilegeLevel;

    /**
     * Sessão desta classe, através dela é realizada a autenticação no método
     * {@link #setVisible(boolean)}
     */
    private AbstractSectionLoginManager.Section currentSection;

    private PrivilegedUISectionWatcher watcher;

    /**
     *
     * @param title Título da janela.
     * @param width Comprimento da janela.
     * @param height Altura da janela.
     * @param isResizeable True se for possível alterar o tamanho da janela com o mouse.
     * @param minAccessPrivilegeLevel Privilégio mínimo para utilizar esta janela, veja {@link User#getPrivilege()} Se
     *                                for igual a {@link User#PRIVILEGE_ANY}, nenhuma sessão é necessária.
     * @param args Argumentos para esta classe, caso haja a necessidade de passar dados.
     */
    protected AbstractPrivilegedUI(@NotNull String title, int width, int height, boolean isResizeable,
                                   int minAccessPrivilegeLevel, @Nullable Object args){

        super(title);

        this.args = args;
        currentSection = null;
        this.minAccessPrivilegeLevel = minAccessPrivilegeLevel;

        setLayout(null);
        setSize(width, height);
        setResizable(isResizeable);


    }

    /**
     * Torna a janela especificada visível, passa a sessão atual para ela e logo após, chama {@link #closeSelf()}.
     * @param ui Janela a ser aberta
     * @returns false caso a sessão desta janela não possua privilégios maiores do que o mínimo para a
     * janela que está para ser aberta
     */
    public boolean startUI(AbstractPrivilegedUI ui){

        if(currentSection.getConnectedUser().getPrivilege() < ui.getMinAccessPrivilegeLevel()) return false;
        ui.setCurrentSection(currentSection);
        ui.setVisible(true);
        closeSelf();

        return true;

    }

    /**
     * Linka uma sessão para esta janela. O efeito será apenas notado ao chamar o método
     * {@link #setVisible(boolean)}, que foi sobrescrita e tornada <b>final</b>. Ali ocorre
     * a autenticação de sessão e não é possível que herançaas modifique o comportamento daquele método.
     * Uma vez chamado este método, não é possível alterar a sessão.
     */
    public void setCurrentSection(AbstractSectionLoginManager.Section section){
        if(currentSection == null) currentSection = section;
    }

    /**
     *
     * @return Mínimo privilégio para esta classe.
     * @see User#getPrivilege()
     */
    public int getMinAccessPrivilegeLevel() {
        return minAccessPrivilegeLevel;
    }

    /**
     *
     * @return Argumentos passados através do construtor da classe.
     */
    public Object getArgs() { return args; }

    /**
     * Fecha esta janela, disparando os eventos em {@link WindowListener}
     */
    public void closeSelf(){

        dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }

    /**
     *
     * @return Sessão desta janela, você pode altera-la através de {@link #setCurrentSection(AbstractSectionLoginManager.Section)}
     */
    public @Nullable AbstractSectionLoginManager.Section getCurrentSection(){
        return currentSection;
    }

    /**
     * Configura esta janela para responder a eventos de falhas de sessão.
     * @param watcher Instância da interface.
     * @see PrivilegedUISectionWatcher
     */
    public void setPrivilegedUISectionWatcher(PrivilegedUISectionWatcher watcher){
        this.watcher = watcher;
    }


    /**
     * <p>Este método foi sobrescrito para introduzir a validação, antes de a janela
     * ser vísivel ou não. Como ela é declarada como <b>final</b>, não é possível alterar o seu comportamento em
     * nenhuma herança. Isto é importante pois sempre será garantida a verificação de sessão.</p>
     * <p>Casos possíveis de falha autenticação (Caso o privilégio mínimo for igual a
     * {@link User#PRIVILEGE_ANY}, nenhum dos passos abaixo é executado):
     * <ul>
     *     <li><b>Sessão não foi passada para esta janela</b>, neste caso a janela não ficará visível, irá
     *     aparecer uma mensagem na saída padrão, pode acontecer caso a função
     *     {@link #setCurrentSection(AbstractSectionLoginManager.Section)} não tenha sido chamada</li>
     *     <li><b>Sessão foi expirada</b>, neste caso a função {@link #closeSelf()} é chamada. Pode acontecer
     *     caso a função {@link AbstractSectionLoginManager.Section#logout()} tenha sido chamada, invalidando a sessão.</li>
     *     <li><b>Privilégio menor que o privilégio mínimo</b>, neste caso é chamado {@link #closeSelf()} e a janela é fechada.</li>
     * </ul></p>
     * <p>Para ouvir a todos estes eventos, use {@link #setPrivilegedUISectionWatcher(PrivilegedUISectionWatcher)}</p>
     * <p><b>Descrição original copiada da classe {@link Window}</b></p>
     * Shows or hides this {@code Window} depending on the value of parameter
     * {@code b}.
     * <p>
     * If the method shows the window then the window is also made
     * focused under the following conditions:
     * <ul>
     * <li> The {@code Window} meets the requirements outlined in the
     *      {@link #isFocusableWindow} method.
     * <li> The {@code Window}'s {@code autoRequestFocus} property is of the {@code true} value.
     * <li> Native windowing system allows the {@code Window} to get focused.
     * </ul>
     * There is an exception for the second condition (the value of the
     * {@code autoRequestFocus} property). The property is not taken into account if the
     * window is a modal dialog, which blocks the currently focused window.
     * <p>
     * Developers must never assume that the window is the focused or active window
     * until it receives a WINDOW_GAINED_FOCUS or WINDOW_ACTIVATED event.
     * @param isVisible  if {@code true}, makes the {@code Window} visible,
     * otherwise hides the {@code Window}.
     * If the {@code Window} and/or its owner
     * are not yet displayable, both are made displayable.  The
     * {@code Window} will be validated prior to being made visible.
     * If the {@code Window} is already visible, this will bring the
     * {@code Window} to the front.<p>
     * If {@code false}, hides this {@code Window}, its subcomponents, and all
     * of its owned children.
     * The {@code Window} and its subcomponents can be made visible again
     * with a call to {@code #setVisible(true)}.
     * @see java.awt.Component#isDisplayable
     * @see java.awt.Component#setVisible
     * @see java.awt.Window#toFront
     * @see java.awt.Window#dispose
     * @see java.awt.Window#setAutoRequestFocus
     * @see java.awt.Window#isFocusableWindow
     */
    @Override
    public final void setVisible(boolean isVisible){

        if(isVisible == true) {

            if (minAccessPrivilegeLevel == User.PRIVILEGE_ANY) {
                super.setVisible(true);
                return;
            }

            if (currentSection == null) {
                if (watcher != null) watcher.onSectionNotRegisteredDetected(this);
                System.err.println("Error: Não há sessão para esta janela, impossível autenticar o usuário, " +
                        "não é possível controlar a visibilidade deste frame...");
                return;
            }

            if (currentSection.isSectionExpired()) {
                if (watcher != null) watcher.onSectionExpiredDetected(this, currentSection);
                System.err.println("Error: Sessão expirada, não é possível usar esta janela.");
                closeSelf();
                return;
            }

            if (currentSection.getConnectedUser().getPrivilege() < minAccessPrivilegeLevel) {
                if (watcher != null) watcher.onSectionNotAllowedDetected(this, currentSection);
                System.err.println("Error: privilégio inválido para esta janela...");
                closeSelf();
                return;
            }

        }

        super.setVisible(isVisible);

    }

}
