package com.mod2.project.authenticatorutils.exception;

import com.mod2.project.authenticatorutils.login.interfaces.Authenticator;

/**
 * Excessão abstrata para tratamento de possíveis erros ou ações inválidas na tentativa de login.
 * @see Authenticator#attemptLogin(String, String)
 */
public abstract class AbstractLoginException extends Exception {

    public AbstractLoginException(String message){
        super(message);

    }

}
