package com.mod2.project.authenticatorutils.exception;

import com.mod2.project.authenticatorutils.User;

/**
 * Gerado quando há uma tentativa de login quando um usuário já está logado.
 */
public class MultiUserConnectionException extends AbstractLoginException {

    private User connectedUser;
    private User failedUser;

    public MultiUserConnectionException(String message, User connectedUser, User failedUser){
        super(message);

        this.failedUser = failedUser;
        this.connectedUser = connectedUser;

    }

    /**
     * @return Retorna o atual usuário conectado.
     * @see #getFailedUser()
     */
    public User getConnectedUser() {
        return connectedUser;
    }

    /**
     * @return Retorna o usuário que falhou devido a existência de outro usuário já conectado.
     * @see #getConnectedUser()
     */
    public User getFailedUser() {
        return failedUser;
    }

}
