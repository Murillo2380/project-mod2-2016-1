package com.mod2.project.ui.registerSector;

import com.mod2.project.authenticatorutils.User;
import com.mod2.project.authenticatorutils.login.AbstractSectionLoginManager;
import com.mod2.project.authenticatorutils.login.interfaces.SectionActionListener;
import com.mod2.project.authenticatorutils.login.interfaces.action.SectionAction;
import com.mod2.project.authenticatorutils.ui.AbstractPrivilegedUI;
import com.mod2.project.db.ModProjectDatabase;
import com.mod2.project.db.entity.member.Dependent;
import com.mod2.project.db.entity.member.Member;
import com.mod2.project.db.entity.member.PendedMember;
import com.mod2.project.ui.utils.InputValidator;
import com.mod2.project.ui.utils.builderHelper.LayoutBuilderHelper;
import com.sun.istack.internal.Nullable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.LinkedList;

/**
 * Tela para o setor de entrada.
 */
public class RegisterMemberPrivilegedUI extends AbstractPrivilegedUI implements WindowListener, ActionListener, SectionActionListener {

    public static final String WINDOW_LABEL = "Cadastrar cliente";
    public static final int WINDOW_WIDTH = 600;
    public static final int WINDOW_HEIGHT = 600;

    private JLabel clientAgeLabel;
    private JLabel clientNameLabel;
    private JLabel clientPhoneLabel;
    private JLabel clientEmailLabel;
    private JLabel clientSalaryLabel;
    private JLabel clientAddressLabel;

    private JTextField clientAgeTextField;
    private JTextField clientNameTextField;
    private JTextField clientEmailTextField;
    private JTextField clientPhoneTextField;
    private JTextField clientSalaryTextField;
    private JTextField clientAddressTextField;

    private JLabel fifthDependentLabel;
    private JLabel thirdDependentLabel;
    private JLabel firstDependentLabel;
    private JLabel secondDependentLabel;
    private JLabel fourthDependentLabel;

    private JTextField fifthDependentTextField;
    private JTextField thirdDependentTextField;
    private JTextField firstDependentTextField;
    private JTextField fourthDependentTextField;
    private JTextField secondDependentTextField;

    private JButton confirmButton;
    private JButton logoutButton;

    /**
     * Construtor que inicializa a janela com título e dimensões padrões.
     */
    public RegisterMemberPrivilegedUI() {
        super(WINDOW_LABEL, WINDOW_WIDTH, WINDOW_HEIGHT, false, User.PRIVILEGE_LOW, null);
        initComponents();
        addWindowListener(this);
    }

    private void initComponents(){
        final int horizontalOffset = 20;
        final int verticalOffset = 20;
        final int textFieldWidth = 200;

        int greaterWidth; // maior comprimento

        JLabel clientSectionLabel = new JLabel("Dados do cliente");
        clientNameLabel = new JLabel("Nome completo");
        clientPhoneLabel = new JLabel("Telefone");
        clientEmailLabel = new JLabel("Email");
        clientSalaryLabel = new JLabel("Salario");
        clientAgeLabel = new JLabel("Idade");
        clientAddressLabel = new JLabel("Endereço");

        LayoutBuilderHelper.offset(clientSectionLabel, 20, LayoutBuilderHelper.OFFSET_VERTICAL);
        LayoutBuilderHelper.offset(clientSectionLabel, 100, LayoutBuilderHelper.OFFSET_HORIZONTAL);

        LayoutBuilderHelper.position(clientNameLabel, clientSectionLabel, LayoutBuilderHelper.POSITION_ALIGN_LEFT, LayoutBuilderHelper.POSITION_BELOW);
        LayoutBuilderHelper.offset(clientNameLabel, verticalOffset, LayoutBuilderHelper.OFFSET_VERTICAL);

        LayoutBuilderHelper.position(clientAgeLabel, clientNameLabel, LayoutBuilderHelper.POSITION_BELOW, LayoutBuilderHelper.POSITION_ALIGN_LEFT);
        LayoutBuilderHelper.offset(clientAgeLabel, verticalOffset, LayoutBuilderHelper.OFFSET_VERTICAL);

        LayoutBuilderHelper.position(clientPhoneLabel, clientAgeLabel,
                LayoutBuilderHelper.POSITION_BELOW, LayoutBuilderHelper.POSITION_ALIGN_LEFT);
        LayoutBuilderHelper.offset(clientPhoneLabel, verticalOffset, LayoutBuilderHelper.OFFSET_VERTICAL);

        LayoutBuilderHelper.position(clientEmailLabel, clientPhoneLabel, LayoutBuilderHelper.POSITION_BELOW, LayoutBuilderHelper.POSITION_ALIGN_LEFT);
        LayoutBuilderHelper.offset(clientEmailLabel, verticalOffset, LayoutBuilderHelper.OFFSET_VERTICAL);

        LayoutBuilderHelper.position(clientSalaryLabel, clientEmailLabel, LayoutBuilderHelper.POSITION_BELOW, LayoutBuilderHelper.POSITION_ALIGN_LEFT);
        LayoutBuilderHelper.offset(clientSalaryLabel, verticalOffset, LayoutBuilderHelper.OFFSET_VERTICAL);

        LayoutBuilderHelper.position(clientAddressLabel, clientSalaryLabel, LayoutBuilderHelper.POSITION_BELOW, LayoutBuilderHelper.POSITION_ALIGN_LEFT);
        LayoutBuilderHelper.offset(clientAddressLabel, verticalOffset, LayoutBuilderHelper.OFFSET_VERTICAL);


        JLabel dependentSectionLabel = new JLabel("Dados dos dependentes");
        firstDependentLabel = new JLabel("Nome do primeiro dependente");
        secondDependentLabel = new JLabel("Nome do segundo dependente");
        thirdDependentLabel = new JLabel("Nome do terceiro dependente");
        fourthDependentLabel = new JLabel("Nome do quarto dependente");
        fifthDependentLabel = new JLabel("Nome do quinto dependente");

        LayoutBuilderHelper.position(dependentSectionLabel, clientAddressLabel, LayoutBuilderHelper.POSITION_ALIGN_LEFT, LayoutBuilderHelper.POSITION_BELOW);
        LayoutBuilderHelper.offset(dependentSectionLabel, verticalOffset * 3, LayoutBuilderHelper.OFFSET_VERTICAL);

        LayoutBuilderHelper.position(firstDependentLabel, dependentSectionLabel, LayoutBuilderHelper.POSITION_ALIGN_LEFT, LayoutBuilderHelper.POSITION_BELOW);
        LayoutBuilderHelper.offset(firstDependentLabel, verticalOffset, LayoutBuilderHelper.OFFSET_VERTICAL);

        LayoutBuilderHelper.position(secondDependentLabel, firstDependentLabel, LayoutBuilderHelper.POSITION_ALIGN_LEFT, LayoutBuilderHelper.POSITION_BELOW);
        LayoutBuilderHelper.offset(secondDependentLabel, verticalOffset, LayoutBuilderHelper.OFFSET_VERTICAL);

        LayoutBuilderHelper.position(thirdDependentLabel, secondDependentLabel, LayoutBuilderHelper.POSITION_ALIGN_LEFT, LayoutBuilderHelper.POSITION_BELOW);
        LayoutBuilderHelper.offset(thirdDependentLabel, verticalOffset, LayoutBuilderHelper.OFFSET_VERTICAL);

        LayoutBuilderHelper.position(fourthDependentLabel, thirdDependentLabel, LayoutBuilderHelper.POSITION_ALIGN_LEFT, LayoutBuilderHelper.POSITION_BELOW);
        LayoutBuilderHelper.offset(fourthDependentLabel, verticalOffset, LayoutBuilderHelper.OFFSET_VERTICAL);

        LayoutBuilderHelper.position(fifthDependentLabel, fourthDependentLabel, LayoutBuilderHelper.POSITION_ALIGN_LEFT, LayoutBuilderHelper.POSITION_BELOW);
        LayoutBuilderHelper.offset(fifthDependentLabel, verticalOffset, LayoutBuilderHelper.OFFSET_VERTICAL);

        greaterWidth = Math.max(clientSectionLabel.getPreferredSize().width, clientNameLabel.getPreferredSize().width); // maior comprimento dos labels para alinhar os textfields
        greaterWidth = Math.max(greaterWidth, clientAgeLabel.getPreferredSize().width);
        greaterWidth = Math.max(greaterWidth, clientPhoneLabel.getPreferredSize().width);
        greaterWidth = Math.max(greaterWidth, clientEmailLabel.getPreferredSize().width);
        greaterWidth = Math.max(greaterWidth, clientSalaryLabel.getPreferredSize().width);
        greaterWidth = Math.max(greaterWidth, clientAddressLabel.getPreferredSize().width);
        greaterWidth = Math.max(greaterWidth, dependentSectionLabel.getPreferredSize().width);
        greaterWidth = Math.max(greaterWidth, firstDependentLabel.getPreferredSize().width);
        greaterWidth = Math.max(greaterWidth, secondDependentLabel.getPreferredSize().width);
        greaterWidth = Math.max(greaterWidth, thirdDependentLabel.getPreferredSize().width);
        greaterWidth = Math.max(greaterWidth, fourthDependentLabel.getPreferredSize().width);
        greaterWidth = Math.max(greaterWidth, fifthDependentLabel.getPreferredSize().width);

        clientNameTextField = new JTextField("Algum Nome");
        clientAgeTextField = new JTextField("25");
        clientPhoneTextField = new JTextField("(xx)xxxx-xxxx");
        clientEmailTextField = new JTextField("algum.emAil@provedor.com.br");
        clientSalaryTextField = new JTextField("999");
        clientAddressTextField = new JTextField("");

        clientNameTextField.setPreferredSize(new Dimension(textFieldWidth, clientNameTextField.getPreferredSize().height)); // Altera o tamanho, dando um novo comprimento e colocando a mesma altura que ele já tinha
        LayoutBuilderHelper.position(clientNameTextField, clientNameLabel, LayoutBuilderHelper.POSITION_ALIGN_LEFT, LayoutBuilderHelper.POSITION_ALIGN_BOTTOM);
        LayoutBuilderHelper.offset(clientNameTextField, greaterWidth + horizontalOffset, LayoutBuilderHelper.OFFSET_HORIZONTAL);


        clientAgeTextField.setPreferredSize(new Dimension(textFieldWidth, clientAgeTextField.getPreferredSize().height));
        LayoutBuilderHelper.position(clientAgeTextField, clientNameTextField, LayoutBuilderHelper.POSITION_ALIGN_LEFT);
        LayoutBuilderHelper.position(clientAgeTextField, clientAgeLabel, LayoutBuilderHelper.POSITION_ALIGN_BOTTOM);


        clientPhoneTextField.setPreferredSize(new Dimension(textFieldWidth, clientPhoneTextField.getPreferredSize().height));
        LayoutBuilderHelper.position(clientPhoneTextField, clientAgeTextField, LayoutBuilderHelper.POSITION_ALIGN_LEFT);
        LayoutBuilderHelper.position(clientPhoneTextField, clientPhoneLabel, LayoutBuilderHelper.POSITION_ALIGN_BOTTOM);


        clientEmailTextField.setPreferredSize(new Dimension(textFieldWidth, clientEmailTextField.getPreferredSize().height));
        LayoutBuilderHelper.position(clientEmailTextField, clientPhoneTextField, LayoutBuilderHelper.POSITION_ALIGN_LEFT);
        LayoutBuilderHelper.position(clientEmailTextField, clientEmailLabel, LayoutBuilderHelper.POSITION_ALIGN_BOTTOM);


        clientSalaryTextField.setPreferredSize(new Dimension(textFieldWidth, clientSalaryTextField.getPreferredSize().height));
        LayoutBuilderHelper.position(clientSalaryTextField, clientEmailTextField, LayoutBuilderHelper.POSITION_ALIGN_LEFT);
        LayoutBuilderHelper.position(clientSalaryTextField, clientSalaryLabel, LayoutBuilderHelper.POSITION_ALIGN_BOTTOM);

        clientAddressTextField.setPreferredSize(new Dimension(textFieldWidth, clientAddressTextField.getPreferredSize().height));
        LayoutBuilderHelper.position(clientAddressTextField, clientSalaryTextField, LayoutBuilderHelper.POSITION_ALIGN_LEFT);
        LayoutBuilderHelper.position(clientAddressTextField, clientAddressLabel, LayoutBuilderHelper.POSITION_ALIGN_BOTTOM);

        firstDependentTextField = new JTextField();
        secondDependentTextField = new JTextField();
        thirdDependentTextField = new JTextField();
        fourthDependentTextField = new JTextField();
        fifthDependentTextField = new JTextField();

        firstDependentTextField.setPreferredSize(new Dimension(textFieldWidth, firstDependentTextField.getPreferredSize().height));
        LayoutBuilderHelper.position(firstDependentTextField, clientAddressTextField, LayoutBuilderHelper.POSITION_ALIGN_LEFT);
        LayoutBuilderHelper.position(firstDependentTextField, firstDependentLabel, LayoutBuilderHelper.POSITION_ALIGN_BOTTOM);

        secondDependentTextField.setPreferredSize(new Dimension(textFieldWidth, secondDependentTextField.getPreferredSize().height));
        LayoutBuilderHelper.position(secondDependentTextField, firstDependentTextField, LayoutBuilderHelper.POSITION_ALIGN_LEFT);
        LayoutBuilderHelper.position(secondDependentTextField, secondDependentLabel, LayoutBuilderHelper.POSITION_ALIGN_BOTTOM);

        thirdDependentTextField.setPreferredSize(new Dimension(textFieldWidth, thirdDependentTextField.getPreferredSize().height));
        LayoutBuilderHelper.position(thirdDependentTextField, secondDependentTextField, LayoutBuilderHelper.POSITION_ALIGN_LEFT);
        LayoutBuilderHelper.position(thirdDependentTextField, thirdDependentLabel, LayoutBuilderHelper.POSITION_ALIGN_BOTTOM);

        fourthDependentTextField.setPreferredSize(new Dimension(textFieldWidth, fourthDependentTextField.getPreferredSize().height));
        LayoutBuilderHelper.position(fourthDependentTextField, thirdDependentTextField, LayoutBuilderHelper.POSITION_ALIGN_LEFT);
        LayoutBuilderHelper.position(fourthDependentTextField, fourthDependentLabel, LayoutBuilderHelper.POSITION_ALIGN_BOTTOM);

        fifthDependentTextField.setPreferredSize(new Dimension(textFieldWidth, fifthDependentTextField.getPreferredSize().height));
        LayoutBuilderHelper.position(fifthDependentTextField, fourthDependentTextField, LayoutBuilderHelper.POSITION_ALIGN_LEFT);
        LayoutBuilderHelper.position(fifthDependentTextField, fifthDependentLabel, LayoutBuilderHelper.POSITION_ALIGN_BOTTOM);

        confirmButton = new JButton("Confirmar");
        LayoutBuilderHelper.position(confirmButton, fifthDependentTextField, LayoutBuilderHelper.POSITION_ALIGN_RIGHT, LayoutBuilderHelper.POSITION_BELOW);
        LayoutBuilderHelper.offset(confirmButton, verticalOffset, LayoutBuilderHelper.OFFSET_VERTICAL);

        logoutButton = new JButton("Logout");
        LayoutBuilderHelper.position(logoutButton, confirmButton, LayoutBuilderHelper.POSITION_TO_LEFT_OF, LayoutBuilderHelper.POSITION_ALIGN_BOTTOM);
        LayoutBuilderHelper.offset(logoutButton, -horizontalOffset, LayoutBuilderHelper.OFFSET_HORIZONTAL);

        add(clientSectionLabel);
        add(clientNameLabel);
        add(clientPhoneLabel);
        add(clientEmailLabel);
        add(clientSalaryLabel);
        add(clientAgeLabel);
        add(clientAddressLabel);

        add(dependentSectionLabel);
        add(firstDependentLabel);
        add(secondDependentLabel);
        add(thirdDependentLabel);
        add(fourthDependentLabel);
        add(fifthDependentLabel);

        add(clientNameTextField);
        add(clientAgeTextField);
        add(clientPhoneTextField);
        add(clientEmailTextField);
        add(clientSalaryTextField);
        add(clientAddressTextField);

        add(firstDependentTextField);
        add(secondDependentTextField);
        add(thirdDependentTextField);
        add(fourthDependentTextField);
        add(fifthDependentTextField);

        add(confirmButton);
        add(logoutButton);

        confirmButton.addActionListener(this);
        logoutButton.addActionListener(this);

    }

    /**
     *
     * @return True se a entrada dos dados estiverem corretos.
     */
    private boolean isInputsValid(){

        boolean isValid = true;

        if(!InputValidator.isValidForRegex(clientNameTextField, InputValidator.REGEX_FOR_NAME)){
            clientNameLabel.setForeground(Color.RED);
            isValid = false;
        }
        else clientNameLabel.setForeground(Color.BLACK);

        if(!InputValidator.isValidForRegex(clientAgeTextField, InputValidator.REGEX_FOR_AGE)){
            clientAgeLabel.setForeground(Color.RED);
            isValid = false;
        }
        else clientAgeLabel.setForeground(Color.BLACK);

        if(!InputValidator.isValidForRegex(clientPhoneTextField, InputValidator.REGEX_FOR_PHONE_NUMBER)){
            clientPhoneLabel.setForeground(Color.RED);
            isValid = false;
        }
        else clientPhoneLabel.setForeground(Color.BLACK);

        if(!InputValidator.isValidForRegex(clientEmailTextField, InputValidator.REGEX_FOR_EMAIL)){
            clientEmailLabel.setForeground(Color.RED);
            isValid = false;
        }
        else clientEmailLabel.setForeground(Color.BLACK);

        if(!InputValidator.isValidForRegex(clientSalaryTextField, InputValidator.REGEX_FOR_FLOAT_NUMBER)){
            clientSalaryLabel.setForeground(Color.RED);
            isValid = false;
        }
        else clientSalaryLabel.setForeground(Color.BLACK);

        if(!InputValidator.isValidForRegex(firstDependentTextField, InputValidator.REGEX_FOR_NULLABLE_NAME)){
            firstDependentLabel.setForeground(Color.RED);
            isValid = false;
        }
        else firstDependentLabel.setForeground(Color.BLACK);

        if(!InputValidator.isValidForRegex(secondDependentTextField, InputValidator.REGEX_FOR_NULLABLE_NAME)){
            secondDependentLabel.setForeground(Color.RED);
            isValid = false;
        }
        else secondDependentLabel.setForeground(Color.BLACK);

        if(!InputValidator.isValidForRegex(thirdDependentTextField, InputValidator.REGEX_FOR_NULLABLE_NAME)){
            thirdDependentLabel.setForeground(Color.RED);
            isValid = false;
        }
        else thirdDependentLabel.setForeground(Color.BLACK);

        if(!InputValidator.isValidForRegex(fourthDependentTextField, InputValidator.REGEX_FOR_NULLABLE_NAME)){
            fourthDependentLabel.setForeground(Color.RED);
            isValid = false;
        }
        else fourthDependentLabel.setForeground(Color.BLACK);

        if(!InputValidator.isValidForRegex(fifthDependentTextField, InputValidator.REGEX_FOR_NULLABLE_NAME)){
            fifthDependentLabel.setForeground(Color.RED);
            isValid = false;
        }
        else fifthDependentLabel.setForeground(Color.BLACK);

        if(clientAddressTextField.getText().equals("")){
            clientAddressLabel.setForeground(Color.RED);
            isValid = false;
        } else clientAddressLabel.setForeground(Color.BLACK);

        return isValid;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if(e.getActionCommand().equals(confirmButton.getText())){

            if(isInputsValid()){
                // entradas são válidas, prosseguir
                boolean sent = sendMemberDataToAnalysis();
                if(sent) JOptionPane.showMessageDialog(this, "Dados enviados!");
                else JOptionPane.showMessageDialog(this, "Erro no banco de dados");
                clearInputs();
            }

        }

        else if(e.getActionCommand().equals(logoutButton.getText()))
            getCurrentSection().logout();

    }

    @Override
    public int onSectionAction(AbstractSectionLoginManager.Section section, SectionAction action) {
        if(action.getAction() == SectionAction.ACTION_LOGOUT) {
            closeSelf();
            return AbstractSectionLoginManager.Section.REQUEST_REMOVE_LISTENER;
        }
        return AbstractSectionLoginManager.Section.REQUEST_NOTHING;
    }

    @Override
    public void windowOpened(WindowEvent e) {
        getCurrentSection().addSectionLogoutListener(RegisterMemberPrivilegedUI.this);
    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    private void clearInputs(){

        clientAgeTextField.setText("");
        clientNameTextField.setText("");
        clientEmailTextField.setText("");
        clientPhoneTextField.setText("");
        clientSalaryTextField.setText("");
        clientAddressTextField.setText("");

        fifthDependentTextField.setText("");
        thirdDependentTextField.setText("");
        firstDependentTextField.setText("");
        fourthDependentTextField.setText("");
        secondDependentTextField.setText("");

    }

    private boolean sendMemberDataToAnalysis(){

        PendedMember newMember = new PendedMember(
                Integer.valueOf(clientAgeTextField.getText()),
                clientNameTextField.getText(),
                Float.valueOf(clientSalaryTextField.getText()),
                clientAddressTextField.getText(),
                clientEmailTextField.getText(),
                clientPhoneTextField.getText(),
                null);

        Dependent[] newDependents = new Dependent[5];
        newDependents[0] = firstDependentTextField.getText().equals("") ? null : new Dependent(Member.INVALID_ID, firstDependentTextField.getText());
        newDependents[1] = secondDependentTextField.getText().equals("") ? null : new Dependent(Member.INVALID_ID, secondDependentTextField.getText());
        newDependents[2] = thirdDependentTextField.getText().equals("") ? null : new Dependent(Member.INVALID_ID, thirdDependentTextField.getText());
        newDependents[3] = fourthDependentTextField.getText().equals("") ? null : new Dependent(Member.INVALID_ID, fourthDependentTextField.getText());
        newDependents[4] = fifthDependentTextField.getText().equals("") ? null : new Dependent(Member.INVALID_ID, fifthDependentTextField.getText());

        for(Dependent d : newDependents)
            if(d != null) newMember.addDependent(d);

        return ModProjectDatabase.getInstance().storeMemberRequest(newMember);

    }

}
