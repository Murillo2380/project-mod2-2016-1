package com.mod2.project.ui.charging;

import com.mod2.project.authenticatorutils.login.AbstractSectionLoginManager;
import com.mod2.project.authenticatorutils.login.interfaces.SectionActionListener;
import com.mod2.project.authenticatorutils.login.interfaces.action.SectionAction;
import com.mod2.project.authenticatorutils.ui.AbstractPrivilegedUI;
import com.mod2.project.db.ModProjectDatabase;
import com.mod2.project.db.entity.member.accepted.AcceptedMember;
import com.mod2.project.db.entity.member.accepted.depts.Debt;
import com.mod2.project.ui.utils.builderHelper.LayoutBuilderHelper;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/**
 * Janela de cobrança de inadimplentes
 */
public class ChargePrivilegedUI extends AbstractPrivilegedUI implements SectionActionListener, WindowListener, ActionListener {

    private static final String WINDOW_LABEL = "Cobrança";

    private static final String BUTTON_LOGOUT = "Logout";
    private static final String BUTTON_GENERATE_PAYMENT = "Gerar boleto";
    private static final String BUTTON_PAY = "Confirmar pagamento";

    private static final int WINDOW_WIDTH = 1200;
    private static final int WINDOW_HEIGHT = 700;

    private DefaultTableModel dtmMembersHaveToPay;
    private DefaultTableModel dtmMembersDontHaveToPay;
    private JTable tableMembersHaveToPay;
    private JTable tableMembersDontHaveToPay;

    private ArrayList<AcceptedMember> membersDontHaveToPay;
    private ArrayList<AcceptedMember> membersHaveToPay;


    public ChargePrivilegedUI() {
        super(WINDOW_LABEL, WINDOW_WIDTH, WINDOW_HEIGHT, false, ModProjectDatabase.SECTOR_CHARGING, null);
        addWindowListener(this);

        AcceptedMember[] membersWithNoDebts = ModProjectDatabase.getInstance().getMembersWithNoDebts();
        if(membersWithNoDebts != null)
            membersDontHaveToPay = new ArrayList<>(Arrays.asList(membersWithNoDebts));
        membersHaveToPay = new ArrayList<>(50);

        AcceptedMember[] a = ModProjectDatabase.getInstance().getAcceptedMembers();
        if(a == null) return;
        ArrayList<AcceptedMember> acceptedMembers = new ArrayList<>(Arrays.asList(a));
        Iterator<AcceptedMember> i = acceptedMembers.iterator();
        AcceptedMember am;
        Debt[] debts;

        boolean hasUnpaidDebt;

        while(i.hasNext()){ // verifica quais sócios ainda não pagaram ou não tem nada pendente.
            am = i.next();

            debts = am.getDebts();

            hasUnpaidDebt = false;

            for(Debt d : debts){

                if(ModProjectDatabase.getInstance().checkDebtValidity(d)) {
                    hasUnpaidDebt = true;
                    break;
                }

            }

            if(hasUnpaidDebt) membersHaveToPay.add(am);

        }

        initComponents();

    }

    private void initComponents(){
        String[] columns = {"Nome", "Valor"};
        String[] nameColumn = {"Nome"};
        String[][] membersHaveToPayRow = null;
        String[][] membersDontHaveToPayRow = null;

        if(membersHaveToPay.size() > 0)
            membersHaveToPayRow = new String[membersHaveToPay.size()][columns.length];
        if(membersDontHaveToPay.size() > 0)
            membersDontHaveToPayRow = new String[membersDontHaveToPay.size()][nameColumn.length];

        for(int i = 0; i < membersHaveToPay.size(); i++){
            membersHaveToPayRow[i][0] = membersHaveToPay.get(i).getName();
            membersHaveToPayRow[i][1] = String.valueOf(membersHaveToPay.get(i).getTotalDebtValue());
        }

        for(int i = 0; i < membersDontHaveToPay.size(); i++)
            membersDontHaveToPayRow[i][0] = membersDontHaveToPay.get(i).getName();


        dtmMembersHaveToPay = new DefaultTableModel(membersHaveToPayRow, columns);
        dtmMembersDontHaveToPay = new DefaultTableModel(membersDontHaveToPayRow, nameColumn);
        tableMembersHaveToPay = new JTable(dtmMembersHaveToPay);
        tableMembersDontHaveToPay = new JTable(dtmMembersDontHaveToPay);

        JScrollPane scrollPane = new JScrollPane(tableMembersHaveToPay);
        JScrollPane scrollPaneTwo = new JScrollPane(tableMembersDontHaveToPay);

        tableMembersDontHaveToPay.setFillsViewportHeight(true);
        tableMembersHaveToPay.setFillsViewportHeight(true);

        LayoutBuilderHelper.offset(scrollPane, 20, LayoutBuilderHelper.OFFSET_HORIZONTAL);
        LayoutBuilderHelper.offset(scrollPane, 20, LayoutBuilderHelper.OFFSET_VERTICAL);

        LayoutBuilderHelper.position(scrollPaneTwo, scrollPane, LayoutBuilderHelper.POSITION_TO_RIGHT_OF, LayoutBuilderHelper.POSITION_ALIGN_TOP);
        LayoutBuilderHelper.offset(scrollPaneTwo, 40, LayoutBuilderHelper.OFFSET_HORIZONTAL);

        JButton generatePayment = new JButton(BUTTON_GENERATE_PAYMENT);
        JButton logout = new JButton(BUTTON_LOGOUT);
        JButton pay = new JButton(BUTTON_PAY);

        LayoutBuilderHelper.position(generatePayment, scrollPaneTwo, LayoutBuilderHelper.POSITION_BELOW, LayoutBuilderHelper.POSITION_ALIGN_RIGHT);
        LayoutBuilderHelper.offset(generatePayment, 20, LayoutBuilderHelper.OFFSET_VERTICAL);

        LayoutBuilderHelper.position(logout, generatePayment, LayoutBuilderHelper.POSITION_ALIGN_BOTTOM, LayoutBuilderHelper.POSITION_TO_LEFT_OF);
        LayoutBuilderHelper.offset(logout, -20, LayoutBuilderHelper.OFFSET_HORIZONTAL);

        LayoutBuilderHelper.position(pay, scrollPane, LayoutBuilderHelper.POSITION_ALIGN_RIGHT, LayoutBuilderHelper.POSITION_BELOW);
        LayoutBuilderHelper.offset(pay, 20, LayoutBuilderHelper.OFFSET_VERTICAL);

        generatePayment.addActionListener(this);
        logout.addActionListener(this);
        pay.addActionListener(this);

        add(scrollPane);
        add(scrollPaneTwo);
        add(generatePayment);
        add(logout);
        add(pay);
    }

    @Override
    public int onSectionAction(AbstractSectionLoginManager.Section section, SectionAction action) {
        if(action.getAction() == SectionAction.ACTION_LOGOUT) {
            closeSelf();
            return AbstractSectionLoginManager.Section.REQUEST_REMOVE_LISTENER;
        }
        return AbstractSectionLoginManager.Section.REQUEST_NOTHING;
    }

    @Override
    public void windowOpened(WindowEvent e) {
        getCurrentSection().addSectionLogoutListener(this);
    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){

            case BUTTON_GENERATE_PAYMENT:
                generatePayment();
                break;

            case BUTTON_LOGOUT:
                getCurrentSection().logout();
                break;

            case BUTTON_PAY:
                pay();
                break;

        }
    }

    private void pay(){
        int row = tableMembersHaveToPay.getSelectedRow();
        if(row == -1) return;

        if(ModProjectDatabase.getInstance().confirmPayment(membersHaveToPay.get(row))) {
            membersHaveToPay.remove(row);
            dtmMembersHaveToPay.removeRow(row);
            JOptionPane.showMessageDialog(this, "Pagamento registrado");
        }

        else JOptionPane.showMessageDialog(this, "Falha ao confimar o pagamento");
    }

    private void generatePayment(){

        int row = tableMembersDontHaveToPay.getSelectedRow();

        if(row == -1) return;

        if(ModProjectDatabase.getInstance().calculatePayment(membersDontHaveToPay.get(row))){
            membersDontHaveToPay.remove(row);
            dtmMembersDontHaveToPay.removeRow(row);
            JOptionPane.showMessageDialog(this, "Pagamento gerado");
        }

        else JOptionPane.showMessageDialog(this, "Erro ao gerar pagamento");

    }

}
