package com.mod2.project.ui.relational;

import com.mod2.project.authenticatorutils.login.AbstractSectionLoginManager;
import com.mod2.project.authenticatorutils.login.interfaces.SectionActionListener;
import com.mod2.project.authenticatorutils.login.interfaces.action.SectionAction;
import com.mod2.project.authenticatorutils.ui.AbstractPrivilegedUI;
import com.mod2.project.db.ModProjectDatabase;
import com.mod2.project.db.entity.member.accepted.AcceptedMember;
import com.mod2.project.ui.utils.builderHelper.LayoutBuilderHelper;
import com.sun.deploy.panel.JHighDPITable;
import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Murillo on 27/06/2016.
 */
public class RelationalPrivilegedUI extends AbstractPrivilegedUI implements SectionActionListener, WindowListener, ActionListener {

    private static final String WINDOW_LABEL = "Relações";
    private static final int WINDOW_WIDTH = 800;
    private static final int WINDOW_HEIGHT = 600;

    private static final String BUTTON_LOGOUT = "logout";
    private static final String BUTTON_SAVE = "Salvar";

    private DefaultTableModel dtm;
    private JTable table;
    private JTextArea communicationInfo;
    private ArrayList<AcceptedMember> members;


    public RelationalPrivilegedUI() {
        super(WINDOW_LABEL, WINDOW_WIDTH, WINDOW_HEIGHT, false, ModProjectDatabase.SECTOR_RELATION, null);

        AcceptedMember[] am = ModProjectDatabase.getInstance().getInadimplentes();
        addWindowListener(this);
        if(am == null) return;
        members = new ArrayList<>(Arrays.asList(am));
        initComponents();

    }

    private void initComponents(){
        if(members.size() == 0) return;

        final String[] columns = {"Cod", "Nome", "Email", "Telefone", "Endereco"};
        String[][] rows = new String[members.size()][columns.length];

        for(int i = 0; i < members.size(); i++){
            rows[i][0] = String.valueOf(members.get(i).getID());
            rows[i][1] = members.get(i).getName();
            rows[i][2] = members.get(i).getEmail();
            rows[i][3] = members.get(i).getPhone();
            rows[i][4] = members.get(i).getAddress();
        }

        dtm = new DefaultTableModel(rows, columns);
        table = new JTable(dtm);
        JScrollPane scrollPane = new JScrollPane(table);
        table.setFillsViewportHeight(true);

        LayoutBuilderHelper.offset(scrollPane, 20, LayoutBuilderHelper.OFFSET_HORIZONTAL);
        LayoutBuilderHelper.offset(scrollPane, 20, LayoutBuilderHelper.OFFSET_VERTICAL);

        communicationInfo = new JTextArea();
        communicationInfo.setLineWrap(true);
        communicationInfo.setPreferredSize(new Dimension(200, 400));

        LayoutBuilderHelper.position(communicationInfo, scrollPane, LayoutBuilderHelper.POSITION_TO_RIGHT_OF, LayoutBuilderHelper.POSITION_ALIGN_TOP);
        LayoutBuilderHelper.offset(communicationInfo, 20, LayoutBuilderHelper.OFFSET_HORIZONTAL);

        JButton saveButton = new JButton(BUTTON_SAVE);
        JButton logoutButton = new JButton(BUTTON_LOGOUT);

        saveButton.addActionListener(this);
        logoutButton.addActionListener(this);

        LayoutBuilderHelper.position(saveButton, communicationInfo, LayoutBuilderHelper.POSITION_BELOW, LayoutBuilderHelper.POSITION_ALIGN_RIGHT);
        LayoutBuilderHelper.offset(saveButton, 20, LayoutBuilderHelper.OFFSET_VERTICAL);

        LayoutBuilderHelper.position(logoutButton, saveButton, LayoutBuilderHelper.POSITION_ALIGN_BOTTOM, LayoutBuilderHelper.POSITION_TO_LEFT_OF);
        LayoutBuilderHelper.offset(logoutButton, -20, LayoutBuilderHelper.OFFSET_HORIZONTAL);

        add(scrollPane);
        add(communicationInfo);
        add(saveButton);
        add(logoutButton);
    }

    @Override
    public int onSectionAction(AbstractSectionLoginManager.Section section, SectionAction action) {
        if(action.getAction() == SectionAction.ACTION_LOGOUT) {
            closeSelf();
            return AbstractSectionLoginManager.Section.REQUEST_REMOVE_LISTENER;
        }

        return AbstractSectionLoginManager.Section.REQUEST_NOTHING;
    }

    @Override
    public void windowOpened(WindowEvent e) {
        getCurrentSection().addSectionLogoutListener(this);
    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){

            case BUTTON_LOGOUT:
                getCurrentSection().logout();
                break;

            case BUTTON_SAVE:
                storeContactAttempt();
                break;

        }
    }

    private void storeContactAttempt(){
        int row = table.getSelectedRow();
        String info = communicationInfo.getText();

        if(row == -1 || communicationInfo.equals("")) return;

        if(ModProjectDatabase.getInstance().storeContactAttempt(members.get(row), info)){
            JOptionPane.showMessageDialog(this, "Armazenado com sucesso");
        }

        else JOptionPane.showMessageDialog(this, "Falha ao armazenar");

    }

}
