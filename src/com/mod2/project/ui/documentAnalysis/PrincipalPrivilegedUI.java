package com.mod2.project.ui.documentAnalysis;

import com.mod2.project.authenticatorutils.User;
import com.mod2.project.authenticatorutils.login.AbstractSectionLoginManager;
import com.mod2.project.authenticatorutils.login.interfaces.SectionActionListener;
import com.mod2.project.authenticatorutils.login.interfaces.action.SectionAction;
import com.mod2.project.authenticatorutils.ui.AbstractPrivilegedUI;
import com.mod2.project.db.ModProjectDatabase;
import com.mod2.project.db.entity.member.PendedMember;
import com.mod2.project.ui.utils.builderHelper.LayoutBuilderHelper;
import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import com.sun.org.apache.xpath.internal.operations.Mod;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;

/**
 * Janela do diretor do setor de análise documental
 */
public class PrincipalPrivilegedUI extends AbstractPrivilegedUI implements SectionActionListener, WindowListener, ActionListener{

    private static final String WINDOWS_LABEL = "Diretor análise documental";
    private static final int WINDOW_WIDTH = 860;
    private static final int WINDOW_HEIGHT = 600;

    private static final String BUTTON_LOGOUT = "Logout";
    private static final String BUTTON_ACCEPT = "aceitar";
    private static final String BUTTON_REFUSE = "rejeitar";

    private JTable table;
    private DefaultTableModel dtm;
    private JTextArea refuseInfoTextArea;
    private ArrayList<PendedMember> pendedMembers;

    /**
     * @param args Membros pendentes para serem aceitos como sócios
     */
    public PrincipalPrivilegedUI(@Nullable ArrayList<PendedMember> args) {
        super(WINDOWS_LABEL, WINDOW_WIDTH, WINDOW_HEIGHT, false, ModProjectDatabase.SECTOR_PRINCIPAL_DOCUMENT_ANALYSIS, args);
        addWindowListener(this);
        this.pendedMembers = args;
        initComponents();
    }

    private void initComponents(){
        final String[] columns = { "Cod", "Nome", "Parecer"};
        final String[][] row = new String[pendedMembers.size()][columns.length];

        for(int i = 0; i < pendedMembers.size(); i++){
            row[i][0] = String.valueOf(pendedMembers.get(i).getID());
            row[i][1] = pendedMembers.get(i).getName();
            row[i][2] = pendedMembers.get(i).getAnalysis();
        }

        dtm = new DefaultTableModel(row, columns);
        table = new JTable(dtm);
        JScrollPane scrollPane = new JScrollPane(table);
        table.setFillsViewportHeight(true);

        LayoutBuilderHelper.offset(scrollPane, 20, LayoutBuilderHelper.OFFSET_HORIZONTAL);
        LayoutBuilderHelper.offset(scrollPane, 20, LayoutBuilderHelper.OFFSET_VERTICAL);

        refuseInfoTextArea = new JTextArea();
        refuseInfoTextArea.setPreferredSize(new Dimension(300, 300));
        refuseInfoTextArea.setLineWrap(true);

        LayoutBuilderHelper.position(refuseInfoTextArea, scrollPane, LayoutBuilderHelper.POSITION_TO_RIGHT_OF, LayoutBuilderHelper.POSITION_ALIGN_TOP);
        LayoutBuilderHelper.offset(refuseInfoTextArea, 20, LayoutBuilderHelper.OFFSET_HORIZONTAL);

        JButton logoutButton = new JButton(BUTTON_LOGOUT);
        JButton acceptButton = new JButton(BUTTON_ACCEPT);
        JButton refuseButton = new JButton(BUTTON_REFUSE);

        LayoutBuilderHelper.position(refuseButton, refuseInfoTextArea, LayoutBuilderHelper.POSITION_ALIGN_RIGHT,LayoutBuilderHelper.POSITION_BELOW);
        LayoutBuilderHelper.offset(refuseButton, 20, LayoutBuilderHelper.OFFSET_VERTICAL);

        LayoutBuilderHelper.position(acceptButton, refuseButton, LayoutBuilderHelper.POSITION_ALIGN_BOTTOM, LayoutBuilderHelper.POSITION_TO_LEFT_OF);
        LayoutBuilderHelper.offset(acceptButton, -20, LayoutBuilderHelper.OFFSET_HORIZONTAL);
        LayoutBuilderHelper.position(logoutButton, acceptButton, LayoutBuilderHelper.POSITION_ALIGN_BOTTOM, LayoutBuilderHelper.POSITION_TO_LEFT_OF);
        LayoutBuilderHelper.offset(logoutButton, -20, LayoutBuilderHelper.OFFSET_HORIZONTAL);

        logoutButton.addActionListener(this);
        acceptButton.addActionListener(this);
        refuseButton.addActionListener(this);

        add(scrollPane);
        add(refuseInfoTextArea);
        add(logoutButton);
        add(acceptButton);
        add(refuseButton);


    }

    @Override
    public int onSectionAction(AbstractSectionLoginManager.Section section, SectionAction action) {
        if(action.getAction() == SectionAction.ACTION_LOGOUT) {
            closeSelf();
            return AbstractSectionLoginManager.Section.REQUEST_REMOVE_LISTENER;
        }
        return AbstractSectionLoginManager.Section.REQUEST_NOTHING;
    }

    @Override
    public void windowOpened(WindowEvent e) {
        getCurrentSection().addSectionLogoutListener(this);
    }

    @Override
    public void windowClosing(WindowEvent e) {
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        switch (e.getActionCommand()){

            case BUTTON_ACCEPT:
                acceptMemberRequest();
                break;

            case BUTTON_REFUSE:
                refuseMemberRequest();
                break;

            case BUTTON_LOGOUT:
                getCurrentSection().logout();
                break;


        }
    }

    private void acceptMemberRequest(){

        int row = table.getSelectedRow();
        if(row == -1) return;

        if(ModProjectDatabase.getInstance().acceptMember(pendedMembers.get(row)))
            JOptionPane.showMessageDialog(this, "Aceito");

        else JOptionPane.showMessageDialog(this, "Problema na aceitação");

        pendedMembers.remove(row);
        dtm.removeRow(row);

    }

    private void refuseMemberRequest(){

        int row = table.getSelectedRow();
        String reason = refuseInfoTextArea.getText();
        if(row == -1 || reason.equals("")) return;


        if(ModProjectDatabase.getInstance().refuseMember(pendedMembers.get(row), reason))
            JOptionPane.showMessageDialog(this, "Recusado");

        else JOptionPane.showMessageDialog(this, "Problema na recusa");
        pendedMembers.remove(row);
        dtm.removeRow(row);

        refuseInfoTextArea.setText("");

    }

}
