package com.mod2.project.ui.documentAnalysis;

import com.mod2.project.authenticatorutils.User;
import com.mod2.project.authenticatorutils.login.AbstractSectionLoginManager;
import com.mod2.project.authenticatorutils.login.interfaces.SectionActionListener;
import com.mod2.project.authenticatorutils.login.interfaces.action.SectionAction;
import com.mod2.project.authenticatorutils.ui.AbstractPrivilegedUI;
import com.mod2.project.db.ModProjectDatabase;
import com.mod2.project.db.entity.member.PendedMember;
import com.mod2.project.ui.utils.builderHelper.LayoutBuilderHelper;
import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;

import static com.sun.xml.internal.fastinfoset.alphabet.BuiltInRestrictedAlphabets.table;

/**
 * Janela do responsável do setor de análise documental
 */
public class DAResponsiblePrivilegedUI extends AbstractPrivilegedUI implements SectionActionListener, WindowListener, ActionListener {

    private static final String WINDOW_TITLE = "Análise documental";
    private static final String BUTTON_SAVE = "Salvar";
    private static final String BUTTON_LOGOUT = "Logout";

    private static final int WINDOW_WIDTH = 780;
    private static final int WINDOW_HEIGHT = 520;

    private JTable table;
    private DefaultTableModel dtm;
    private JTextArea analysisTextArea;
    private ArrayList<PendedMember> pendedMembers;

    /**
     * Construtor que inicializa a janela com título e dimensões padrões
     * @param pendedMembers Dados das pessoas que ainda precisam ser analisados.
     */
    public DAResponsiblePrivilegedUI(ArrayList<PendedMember> pendedMembers) {
        super(WINDOW_TITLE, WINDOW_WIDTH, WINDOW_HEIGHT, false, ModProjectDatabase.SECTOR_DOCUMENT_ANALYSIS, pendedMembers);
        this.pendedMembers = pendedMembers;
        initComponents();
        addWindowListener(this);
    }

    private void initComponents(){
        final String[] columns = {"Cod", "Nome", "Número de dependentes", "Idade", "Salario"};
        String[][] row = new String[pendedMembers.size()][columns.length];

        for(int i = 0; i < pendedMembers.size(); i++){
            row[i][0] = String.valueOf(pendedMembers.get(i).getID());
            row[i][1] = pendedMembers.get(i).getName();
            row[i][2] = String.valueOf(pendedMembers.get(i).getNumDependents());
            row[i][3] = String.valueOf(pendedMembers.get(i).getAge());
            row[i][4] = String.valueOf(pendedMembers.get(i).getSalary());
        }

        dtm = new DefaultTableModel(row, columns);
        table = new JTable(dtm);
        JScrollPane scrollPane = new JScrollPane(table);
        table.setFillsViewportHeight(true);
        LayoutBuilderHelper.offset(scrollPane, 20, LayoutBuilderHelper.OFFSET_VERTICAL);
        LayoutBuilderHelper.offset(scrollPane, 20, LayoutBuilderHelper.OFFSET_HORIZONTAL);

        analysisTextArea = new JTextArea();
        analysisTextArea.setLineWrap(true);
        analysisTextArea.setPreferredSize(new Dimension(240, 400));
        LayoutBuilderHelper.position(analysisTextArea, scrollPane, LayoutBuilderHelper.POSITION_TO_RIGHT_OF, LayoutBuilderHelper.POSITION_ALIGN_TOP);
        LayoutBuilderHelper.offset(analysisTextArea, 20, LayoutBuilderHelper.OFFSET_HORIZONTAL);

        JButton logoutButton = new JButton(BUTTON_LOGOUT);
        JButton save = new JButton(BUTTON_SAVE);
        save.addActionListener(this);
        logoutButton.addActionListener(this);

        LayoutBuilderHelper.position(save, analysisTextArea, LayoutBuilderHelper.POSITION_ALIGN_RIGHT, LayoutBuilderHelper.POSITION_BELOW);
        LayoutBuilderHelper.offset(save, 20, LayoutBuilderHelper.OFFSET_VERTICAL);
        LayoutBuilderHelper.position(logoutButton, save, LayoutBuilderHelper.POSITION_ALIGN_BOTTOM, LayoutBuilderHelper.POSITION_TO_LEFT_OF);
        LayoutBuilderHelper.offset(logoutButton, -20, LayoutBuilderHelper.OFFSET_HORIZONTAL);

        add(scrollPane);
        add(analysisTextArea);
        add(save);
        add(logoutButton);
    }

    @Override
    public int onSectionAction(AbstractSectionLoginManager.Section section, SectionAction action) {
        if(action.getAction() == SectionAction.ACTION_LOGOUT) {
            closeSelf();
            return AbstractSectionLoginManager.Section.REQUEST_REMOVE_LISTENER;
        }
        return AbstractSectionLoginManager.Section.REQUEST_NOTHING;
    }

    @Override
    public void windowOpened(WindowEvent e) {
        getCurrentSection().addSectionLogoutListener(this);
    }

    @Override
    public void windowClosing(WindowEvent e) {
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        switch(e.getActionCommand()){

            case BUTTON_SAVE:

                storeAnalysis();

                break;

            case BUTTON_LOGOUT:
                getCurrentSection().logout();
                break;

        }

    }

    private void storeAnalysis(){
        int row = table.getSelectedRow();
        String analysis = analysisTextArea.getText();
        if(row < 0 || analysis.equals("")) return;

        if(ModProjectDatabase.getInstance().storeMemberAnalysis(pendedMembers.get(row), analysis))
            JOptionPane.showMessageDialog(this, "Análise realizada com sucesso");

        else JOptionPane.showMessageDialog(this, "Armazenamento falhou");

        analysisTextArea.setText("");
        dtm.removeRow(row);
        pendedMembers.remove(row);
    }

}
