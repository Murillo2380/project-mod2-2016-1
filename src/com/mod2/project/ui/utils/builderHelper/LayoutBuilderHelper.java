package com.mod2.project.ui.utils.builderHelper;

import javax.swing.*;

/**
 * Classe que facilita a construção de layouts absolutos.
 */
public class LayoutBuilderHelper {

    /**
     * Argumento para {@link #position(JComponent, JComponent, int...)}, posiciona o primeiro
     * componente a baixo do segundo componente.
     */
    public static final int POSITION_BELOW = 0;
    /**
     * Argumento para {@link #position(JComponent, JComponent, int...)}, posiciona o primeiro
     * componente a direita do segundo componente.
     */
    public static final int POSITION_TO_RIGHT_OF = 1;
    /**
     * Argumento para {@link #position(JComponent, JComponent, int...)}, posiciona o primeiro
     * componente a esquerda do segundo componente.
     */
    public static final int POSITION_TO_LEFT_OF = 2;
    /**
     * Argumento para {@link #position(JComponent, JComponent, int...)}, posiciona o primeiro
     * componente a cima do segundo componente.
     */
    public static final int POSITION_ABOVE = 3;
    /**
     * Argumento para {@link #position(JComponent, JComponent, int...)}, alinha o lado direito do
     * primeiro componente ao lado direito do segundo componente.
     */
    public static final int POSITION_ALIGN_RIGHT = 4;
    /**
     * Argumento para {@link #position(JComponent, JComponent, int...)}, alinha o lado esquerdo do
     * primeiro componente ao lado esquerdo do segundo componente.
     */
    public static final int POSITION_ALIGN_LEFT = 5;

    /**
     * Argumento para {@link #position(JComponent, JComponent, int...)}, alinha o topo do
     * primeiro componente ao topo do segundo componente.
     */
    public static final int POSITION_ALIGN_TOP = 8;

    /**
     * Argumento para {@link #position(JComponent, JComponent, int...)}, alinha o canto inferior do
     * primeiro componente ao canto inferior do segundo componente.
     */
    public static final int POSITION_ALIGN_BOTTOM = 9;

    /**
     * Argumento para {@link #offset(JComponent, int, int)}
     */
    public static final int OFFSET_VERTICAL = 6;

    /**
     * Argumento para {@link #offset(JComponent, int, int)}
     */
    public static final int OFFSET_HORIZONTAL = 7;

    private LayoutBuilderHelper(){}


    /**
     *
     * @param which Componente a ser movido
     * @param target Componente base para mover o primeiro componente.
     * @param positionArgs Argumentos de posição, uma série de argumentos podem ser passados para esta função
     *                     e a movimentação do componente ocorrerá na ordem em que os argumentos foram passados
     *                     para esta função. Vale notar que a posicionamento ocorre de maneira direta, isto é,
     *                     o primeiro componente não "segue" o componente alvo, ele é apenas posicionado uma
     *                     única vez, podendo ser posicionado novamente caso necessário.
     *
     * @see #POSITION_ABOVE
     * @see #POSITION_BELOW
     * @see #POSITION_ALIGN_LEFT
     * @see #POSITION_ALIGN_RIGHT
     * @see #POSITION_ALIGN_TOP
     * @see #POSITION_ALIGN_BOTTOM
     * @see #POSITION_TO_LEFT_OF
     * @see #POSITION_TO_RIGHT_OF
     */
    public static void position(JComponent which, JComponent target, int... positionArgs){

        int newX = which.getBounds().x;
        int newY = which.getBounds().y;

        for(int arg : positionArgs){

           switch (arg){

               case POSITION_ABOVE:

                   newY = target.getBounds().y  - which.getPreferredSize().height;

                   break;

               case POSITION_BELOW:

                   newY = target.getBounds().y + target.getPreferredSize().height;

                   break;

               case POSITION_TO_RIGHT_OF:

                   newX = target.getBounds().x + target.getPreferredSize().width;

                   break;

               case POSITION_TO_LEFT_OF:

                   newX = target.getBounds().x - which.getPreferredSize().width;

                   break;

               case POSITION_ALIGN_LEFT:

                   newX = target.getBounds().x;

                   break;

               case POSITION_ALIGN_RIGHT:

                   newX = target.getBounds().x + target.getPreferredSize().width - which.getPreferredSize().width;

                   break;

               case POSITION_ALIGN_TOP:

                   newY = target.getBounds().y;

                   break;

               case POSITION_ALIGN_BOTTOM:

                   newY = target.getBounds().y + target.getPreferredSize().height - which.getPreferredSize().height;

                   break;

           }

        }

        which.setBounds(newX, newY, which.getPreferredSize().width, which.getPreferredSize().height);

    }

    /**
     * Desloca o componente ao valor atribuído, na vertical ou horizontal.
     * @param which Componente a ser deslocado
     * @param value Valor do deslocamento
     * @param orientation Sentido aonde ocorrerá o deslocamento, podendo ser vertical
     *                    ou horizontal, através dos respectivos valores {@link #OFFSET_VERTICAL} E
     *                    {@link #OFFSET_HORIZONTAL}
     */
    public static void offset(JComponent which, int value, int orientation){

        if(orientation == OFFSET_VERTICAL){
            which.setBounds(which.getBounds().x, which.getBounds().y + value,
                    which.getPreferredSize().width, which.getPreferredSize().height);
            return;
        }

        which.setBounds(which.getBounds().x + value, which.getBounds().y,
                which.getPreferredSize().width, which.getPreferredSize().height);

    }


}
