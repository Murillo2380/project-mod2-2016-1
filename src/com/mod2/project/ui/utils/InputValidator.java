package com.mod2.project.ui.utils;

import com.sun.istack.internal.NotNull;

import javax.swing.*;
import java.util.regex.Pattern;

/**
 * Classe para auxiliar validação de entradas a partir de expressões regulares.
 */
public class InputValidator{

    /**
     * Padrão para identificar nomes, onde cada nome deve começar com letra maiúscula. <b>Aceita: Algum Nome</b>, <b>rejeita: algum nome</b>
     */
    public static final Pattern REGEX_FOR_NAME = Pattern.compile("[A-Z][a-z]+(\\s[A-Z][a-z]*)*");
    /**
     * Mesmo que {@link #REGEX_FOR_NAME}, porém aceita palavra vazia.
     */
    public static final Pattern REGEX_FOR_NULLABLE_NAME = Pattern.compile("([A-Z][a-z]+(\\s[A-Z][a-z]*)*)?");
    /**
     * Aceita apenas números
     */
    public static final Pattern REGEX_FOR_NUMBER = Pattern.compile("\\d+");/**
     * Aceita números inteiros ou quebrados. Exemplo: 123 ou 123.567, rejeita: .123 ou 123.456.789
     */
    public static final Pattern REGEX_FOR_FLOAT_NUMBER = Pattern.compile("\\d+(\\.\\d+)?");
    /**
     * Aceita apenas email, contendo texto@texto seguidos de .com ou .com.br
     */
    public static final Pattern REGEX_FOR_EMAIL = Pattern.compile("[a-zA-Z][a-zA-Z\\.]*@[a-zA-Z]+\\.com(\\.br)?");
    /**
     * Aceita apenas números no formato: (xx)yxxxx-xxxx, onde o y é opcional.
     */
    public static final Pattern REGEX_FOR_PHONE_NUMBER = Pattern.compile("[(]\\d\\d[)]\\d?\\d{4}-\\d{4}");
    /**
     * Aceita apenas idades, podendo conter um, dois ou três dígitos.
     */
    public static final Pattern REGEX_FOR_AGE = Pattern.compile("\\d{1,3}");


    private InputValidator(){}

    /**
     *
     * @param inputToValidate Input para ser validado
     * @param regex Expressão regular para validar o input
     * @return True se o texto está de acordo com a expressão regular.
     * @see #REGEX_FOR_AGE
     * @see #REGEX_FOR_EMAIL
     * @see #REGEX_FOR_NAME
     * @see #REGEX_FOR_NULLABLE_NAME
     * @see #REGEX_FOR_PHONE_NUMBER
     * @see #REGEX_FOR_NUMBER
     * @see #REGEX_FOR_FLOAT_NUMBER
     * @see #isValidForRegex(JTextField, String)
     */
    public static boolean isValidForRegex(@NotNull JTextField inputToValidate, Pattern regex){
        return inputToValidate != null && regex.matcher(inputToValidate.getText()).matches();
    }

    /**
     *
     * @param inputToValidate Input para ser validado
     * @param regex Expressão regular para validar o input
     * @return True se o texto está de acordo com a expressão regular.
     * @see #isValidForRegex(JTextField, Pattern)
     */
    public static boolean isValidForRegex(JTextField inputToValidate, String regex){
        return inputToValidate != null && inputToValidate.getText().matches(regex);
    }

}
