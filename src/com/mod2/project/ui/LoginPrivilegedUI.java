package com.mod2.project.ui;

import com.mod2.project.ProjectSystem;
import com.mod2.project.authenticatorutils.User;
import com.mod2.project.authenticatorutils.login.interfaces.AuthenticatorListener;
import com.mod2.project.authenticatorutils.login.AbstractSectionLoginManager;
import com.mod2.project.db.ModProjectDatabase;
import com.mod2.project.authenticatorutils.ui.AbstractPrivilegedUI;
import com.mod2.project.db.entity.member.PendedMember;
import com.mod2.project.db.entity.member.accepted.AcceptedMember;
import com.mod2.project.ui.charging.ChargePrivilegedUI;
import com.mod2.project.ui.documentAnalysis.DAResponsiblePrivilegedUI;
import com.mod2.project.ui.documentAnalysis.PrincipalPrivilegedUI;
import com.mod2.project.ui.registerSector.RegisterMemberPrivilegedUI;
import com.mod2.project.ui.relational.RelationalPrivilegedUI;
import com.sun.istack.internal.NotNull;
import com.sun.org.apache.xpath.internal.operations.Mod;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Cria uma janela de login.
 */
public class LoginPrivilegedUI extends AbstractPrivilegedUI implements AuthenticatorListener, ActionListener{

    public static final String WINDOW_LABEL = "Login";

    private JButton loginButton;
    private JTextField loginField;
    private ModProjectDatabase manager;
    private JPasswordField passwordField;

    private ProjectSystem system;

    /**
     *
     * @param width Comprimento.
     * @param height Altura.
     */
    public LoginPrivilegedUI(ProjectSystem system, final int width, final int height){
        super(WINDOW_LABEL, width, height, false, User.PRIVILEGE_ANY, null);

        initComponents();

        manager = ModProjectDatabase.getInstance();
        manager.addAuthenticatorListener(this);

        this.system = system;

    }

    /**
     * Inicializa todos os componentes visíveis.
     */
    private void initComponents(){

        int verticalOffset = 20;
        int horizontalOffset = 20;
        int greaterWidth;

        JLabel loginLabel = new JLabel("Usuário");
        loginLabel.setBounds(100, 80, loginLabel.getPreferredSize().width, loginLabel.getPreferredSize().height);

        JLabel passwordLabel = new JLabel("Senha");
        passwordLabel.setBounds(loginLabel.getBounds().x, loginLabel.getBounds().y + loginLabel.getBounds().height + verticalOffset,
                passwordLabel.getPreferredSize().width, passwordLabel.getPreferredSize().height);

        greaterWidth = loginLabel.getBounds().width > passwordLabel.getBounds().width ? loginLabel.getBounds().width : passwordLabel.getBounds().width;
        greaterWidth += horizontalOffset;

        loginField = new JTextField();
        loginField.setPreferredSize(new Dimension(100, loginField.getPreferredSize().height));
        loginField.setBounds(loginLabel.getBounds().x + greaterWidth, loginLabel.getBounds().y,
                loginField.getPreferredSize().width, loginField.getPreferredSize().height);

        passwordField = new JPasswordField();
        passwordField.setPreferredSize(new Dimension(100, passwordField.getPreferredSize().height));
        passwordField.setBounds(passwordLabel.getBounds().x + greaterWidth, passwordLabel.getBounds().y,
                passwordField.getPreferredSize().width, passwordField.getPreferredSize().height);

        loginButton = new JButton("Entrar");
        loginButton.setPreferredSize(new Dimension(100, loginButton.getPreferredSize().height));

        loginButton.setBounds(passwordField.getBounds().x, passwordField.getBounds().y + loginButton.getPreferredSize().height + verticalOffset,
                loginButton.getPreferredSize().width, loginButton.getPreferredSize().height);

        loginButton.addActionListener(this);

        add(loginLabel);
        add(passwordLabel);
        add(loginField);
        add(passwordField);
        add(loginButton);

    }


    @Override
    public void actionPerformed(ActionEvent e) {

        if(e.getActionCommand().equals(loginButton.getText())){
            String login = loginField.getText();
            String password = String.valueOf(passwordField.getPassword());
            manager.attemptLogin(login, password);
        }

    }

    /**
     * Verifica quem logou e abre uma interface de acordo com o setor.
     * @param section Sessão de usuário que logou.
     * @return True se alguma janela for aberta.
     */
    private boolean openForSector(@NotNull AbstractSectionLoginManager.Section section){

        PendedMember[] pendedMembers;

        switch (section.getConnectedUser().getPrivilege()){

            case ModProjectDatabase.SECTOR_ENTRANCE:

                if(ModProjectDatabase.getInstance().getNumAcceptedMembers() <= 20)
                    startUI(new RegisterMemberPrivilegedUI());

                else {
                    showMessage("Não é possível aceitar mais pedidos");
                    return false;
                }

                break;

            case ModProjectDatabase.SECTOR_CHARGING:
                startUI(new ChargePrivilegedUI());
                break;

            case ModProjectDatabase.SECTOR_DOCUMENT_ANALYSIS:
                pendedMembers = ModProjectDatabase.getInstance().getMembersRequest();

                if(pendedMembers == null) {
                    showMessage("Não há ninguém para ser analisado");
                    return false;
                }

                startUI(new DAResponsiblePrivilegedUI(new ArrayList<>(Arrays.asList(pendedMembers))));

                break;

            case ModProjectDatabase.SECTOR_PRINCIPAL_DOCUMENT_ANALYSIS:
                pendedMembers = ModProjectDatabase.getInstance().getAnalysedMembersRequest();
                if(pendedMembers == null){
                    showMessage("Não há ninguém para ser aceito");
                    return false;
                }

                else startUI(new PrincipalPrivilegedUI(new ArrayList<>(Arrays.asList(pendedMembers))));

                break;

            case ModProjectDatabase.SECTOR_RELATION:
                startUI(new RelationalPrivilegedUI());
                break;

            case ModProjectDatabase.SECTOR_RESERVE:

                break;

            case ModProjectDatabase.SECTOR_PRINCIPAL:

                break;

        }
        return true;
    }


    @Override
    public int onLoginAttemptSuccess(AbstractSectionLoginManager.Section section) {

        system.userConnected(section); // avisa ao sistema que um login foi bem sucedido.
        System.out.println("Sucesso " + String.valueOf(section.getConnectedUser()));
        setCurrentSection(section);

        if(!openForSector(section)) {
            section.logout();
            closeSelf();
        }

        return ModProjectDatabase.REQUEST_REMOVE_LISTENER;
    }

    @Override
    public int onLoginAttemptFailed(String user, String password) {
        JOptionPane.showMessageDialog(this, "Usuário ou senha incorretos", "Erro na autenticação", JOptionPane.ERROR_MESSAGE);
        passwordField.setText("");
        loginField.setText("");
        return ModProjectDatabase.REQUEST_NOTHING;
    }

    private void showMessage(String message){
        JOptionPane.showMessageDialog(this, message);
    }

}
