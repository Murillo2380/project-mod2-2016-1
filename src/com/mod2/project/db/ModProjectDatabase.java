package com.mod2.project.db;

import com.mod2.project.authenticatorutils.User;
import com.mod2.project.authenticatorutils.login.AbstractSectionLoginManager;
import com.mod2.project.authenticatorutils.login.interfaces.DatabaseConnection;
import com.mod2.project.authenticatorutils.login.interfaces.AuthenticatorListener;
import com.mod2.project.db.entity.*;
import com.mod2.project.db.entity.member.Dependent;
import com.mod2.project.db.entity.member.accepted.AcceptedMember;
import com.mod2.project.db.entity.member.Member;
import com.mod2.project.db.entity.member.PendedMember;
import com.mod2.project.db.entity.member.RefusedMember;
import com.mod2.project.db.entity.member.accepted.depts.Debt;
import com.mod2.project.db.entity.member.accepted.depts.PaidDebt;
import com.mod2.project.db.entity.member.accepted.reservation.Attraction;
import com.mod2.project.db.entity.member.accepted.reservation.Guest;
import com.mod2.project.db.entity.member.accepted.reservation.Reservation;
import com.sun.corba.se.impl.orb.PrefixParserAction;
import com.sun.corba.se.spi.orbutil.fsm.Guard;
import com.sun.crypto.provider.RSACipher;
import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Created by Murillo on 04/04/2016.
 */
public class ModProjectDatabase extends AbstractSectionLoginManager implements DatabaseConnection {

    /**
     * Ao invés de chamar a função {@link #removeAuthenticatorListener(AuthenticatorListener)} dentro da implementação
     * da função {@link AuthenticatorListener#onLoginAttemptSuccess(Section)}} ou {@link AuthenticatorListener#onLoginAttemptFailed(String, String)},
     * use este valor como retorno para estes dois últimos métodos,
     * de modo a remover o escutador de dentro da implementação, de maneira segura.
     */
    public static final int REQUEST_REMOVE_LISTENER = 171;
    /**
     * Se usar este valor como retorno dos métodos {@link AuthenticatorListener#onLoginAttemptSuccess(Section)}}
     * ou {@link AuthenticatorListener#onLoginAttemptFailed(String, String)}, nenhuma operação em especial
     * é realizada.
     * @see #REQUEST_REMOVE_LISTENER
     */
    public static final int REQUEST_NOTHING = 0;

    /**
     *
     */
    public static final int SECTOR_PRINCIPAL = 58;

    /**
     *
     */
    public static final int SECTOR_PRINCIPAL_DOCUMENT_ANALYSIS = 59;

    /**
     *
     */
    public static final int SECTOR_CHARGING  = 60;

    /**
     *
     */
    public static final int SECTOR_DOCUMENT_ANALYSIS = 61;

    /**
     *
     */
    public static final int SECTOR_RELATION = 62;

    /**
     *
     */
    public static final int SECTOR_ENTRANCE = 63;

    /**
     *
     */
    public static final int SECTOR_RESERVE = 64;




    private Connection connection = null;

    /**
     * Escutador para estados de login.
     * @see AuthenticatorListener
     */
    private LinkedList<AuthenticatorListener> listenerList = new LinkedList<>();

    private static final class ModProjectDatabaseHolder{
        private static final ModProjectDatabase INSTANCE = new ModProjectDatabase();
    }

    private ModProjectDatabase(){

    }

    public static ModProjectDatabase getInstance(){
        return ModProjectDatabaseHolder.INSTANCE;
    }

    /**
     * Adiciona um escutador em uma lista de escutadores, possibilitando que várias classes implemente {@link AuthenticatorListener}
     * @param listener Instância do escutador.
     * @see #removeAuthenticatorListener(AuthenticatorListener)
     * @see AuthenticatorListener
     */
    public void addAuthenticatorListener(AuthenticatorListener listener) {

        if(!listenerList.contains(listener))
            listenerList.add(listener);

    }

    /**
     * Remove um escutador específico. Se a sua classe implementa {@link AuthenticatorListener}, você não deve
     * chamar esta função dentro da implementação desta interface pois pode ocorrer um comportamento inesperado,
     * ao invés disto, faça com que as funções retorne {@link #REQUEST_REMOVE_LISTENER} ou {@link #REQUEST_NOTHING}
     * para que a remoção ocorra de maneira segura ou que ela não ocorra.
     * @param listener Instância a ser removida.
     * @see #addAuthenticatorListener(AuthenticatorListener)
     * @see AuthenticatorListener
     */
    public void removeAuthenticatorListener(AuthenticatorListener listener){
        listenerList.remove(listener);
    }

    /**
     * Tenta realizar uma autenticação. Caso o login tenha sido bem sucedido, a função
     * {@link AuthenticatorListener#onLoginAttemptSuccess(Section)} é chamada, caso o login tenha sido
     * mal sucedido, a função {@link AuthenticatorListener#onLoginAttemptFailed(String, String)} é chamada. Você
     * pode escutar a estes eventos através do método {@link #addAuthenticatorListener(AuthenticatorListener)}
     * @param user Usuário para autenticação.
     * @param password senha
     * @return Sessão do login se encontrar um usuário com o nome e senha, null caso contrário.
     */
    @Override
    public Section attemptLogin(@NotNull String user, @NotNull String password)  {

        if(connection == null){
            System.err.println("Error: Sem conexão com o banco, não é possível realizar a autenticação...");
            return  null;
        }

        if(user.equals("") || password.equals("")){
            Iterator<AuthenticatorListener> i = listenerList.iterator();

            while(i.hasNext())
                i.next().onLoginAttemptFailed(user, password);

            return null;
        }

        final String query = "SELECT privilegio FROM LOGIN NATURAL JOIN setor WHERE USUARIO = ? AND SENHA = ?; ";

        try {
            PreparedStatement stm = connection.prepareStatement(query);
            stm.setString(1, user);
            stm.setString(2, password);
            ResultSet rs = stm.executeQuery();

            if(!rs.next()){ // nenhum resultado.

                Iterator<AuthenticatorListener> i = listenerList.iterator();

                while(i.hasNext())
                    if(i.next().onLoginAttemptFailed(user, password) == REQUEST_REMOVE_LISTENER)
                        i.remove();


                return null;
            }

            int userPrivilege = rs.getInt(1);

            Section section = newSection(new User(rs.getString(1), userPrivilege));
            Iterator<AuthenticatorListener> i = listenerList.iterator();

            while(i.hasNext())
                if(i.next().onLoginAttemptSuccess(section) == REQUEST_REMOVE_LISTENER)
                    i.remove();

            rs.close();

            return section;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Connection connectDB(String sqlDriver, String url, String user, String password) {

        if(connection != null) return connection;

        try {

            Class.forName(sqlDriver);
            connection = DriverManager.getConnection(url, user, password);


        } catch(ClassNotFoundException e){

            System.out.println("Classe não encontrada");
            return null;

        } catch (SQLException e){

            e.printStackTrace();

        }

        return connection;
    }

    @Override
    public void disconnectDB() {

        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean isConnected(){
        return connection != null;
    }

    /**
     * Armazena o membro que fez a requesição para ser sócio.
     * @param member Membro a ser armazenado no banco de dados.
     * @return False se não foi possível armazenar o dado no banco.
     */
    public boolean storeMemberRequest(@NotNull PendedMember member){

        if(!isConnected()){
            System.err.println("Não há conexão com o banco de dados");
            return false;
        }

        final int salaryColumn = 1;
        final int phoneColumn = 2;
        final int addressColumn = 3;
        final int emailColumn = 4;
        final int nameColumn = 5;
        final int ageColumn = 6;

        String query = "INSERT INTO Clientes (salario, telefone, endereco, email, dataaprovacao, motivorecusa, nome, idade, parecer) " +
                " values (?, ?, ?, ?, NULL, NULL, ?, ?, NULL);";

        try {

            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setFloat(salaryColumn, member.getSalary());
            preparedStatement.setString(phoneColumn, member.getPhone());
            preparedStatement.setString(addressColumn, member.getAddress());
            preparedStatement.setString(emailColumn, member.getEmail());
            preparedStatement.setString(nameColumn, member.getName());
            preparedStatement.setInt(ageColumn, member.getAge());

            preparedStatement.executeUpdate();

            Dependent[] dependents = member.getDependents();
            if(dependents == null) return true;

            query = "SELECT idcliente FROM Clientes ORDER BY idcliente DESC LIMIT 1";
            preparedStatement = connection.prepareStatement(query);

            ResultSet rs = preparedStatement.executeQuery();
            if(!rs.next()) return false;
            int memberID = rs.getInt(1);

            query = "INSERT INTO DEPENDENTE (nome, idcliente) VALUES (?, ?)";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(2, memberID);

            for(Dependent d : dependents){
                preparedStatement.setString(1, d.getName());
                preparedStatement.executeUpdate();
            }

        }catch(SQLException e){
            e.printStackTrace();
            return false;
        }

        return true;

    }

    /**
     * Aceita um membro como sócio.
     * @param member Membro a ser aceito.
     */
    public boolean acceptMember(@NotNull PendedMember member){

        if(!isConnected()){
            System.err.println("Não há conexão com o banco de dados");
            return false;
        }

        if(member.getID() == DBEntity.INVALID_ID){
            System.err.println("ID Inválido para membro");
            return false;
        }

        final String query = "UPDATE clientes SET dataaprovacao = now(), motivorecusa = NULL WHERE IDCliente = ?";

        try{
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, member.getID());
            preparedStatement.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
            return false;
        }

        return true;

    }

    /**
     * Recusa um determinado membro como sócio.
     * @param member Membro a ser recusado.
     * @param reason Motivo da recusa.
     */
    public boolean refuseMember(@NotNull Member member, @NotNull String reason){

        if(!isConnected()){
            System.err.println("Não há conexão com o banco de dados");
            return false;
        }

        final String query = " UPDATE clientes SET dataaprovacao = NULL, motivorecusa = ? WHERE IDCliente = ? ";

        try{
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, reason);
            preparedStatement.setInt(2, member.getID());
            preparedStatement.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }

        return true;

    }

    /**
     *
     * @return Todos os membros que aguardam pela aceitação para ser sócio e ainda não passaram pelo setor de análise.
     * @see #getAnalysedMembersRequest()
     */
    public @Nullable PendedMember[] getMembersRequest(){

        if(!isConnected()){
            System.err.println("Não há conexão com o banco de dados");
            return null;
        }

        PendedMember pm;
        ArrayList<PendedMember> pms = new ArrayList<>(50);
        final String query = " SELECT IDCliente, idade, nome, salario, endereco, email, telefone FROM Clientes " +
                "WHERE dataAprovacao IS NULL AND motivorecusa IS NULL AND PARECER IS NULL";

        final int columnID= 1;
        final int columnAge = 2;
        final int columnName = 3;
        final int columnSalary = 4;
        final int columnAddress = 5;
        final int columnEmail = 6;
        final int columnPhone = 7;

        try{
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()){
                pm = new PendedMember(rs.getInt(columnID), rs.getInt(columnAge), rs.getString(columnName),
                        rs.getFloat(columnSalary), rs.getString(columnAddress), rs.getString(columnEmail),
                        rs.getString(columnPhone), null);
                fillDependentsFromMember(pm);
                pms.add(pm);
            }

            rs.close();

        }catch(SQLException e){
            e.printStackTrace();
        }


        if(pms.size() == 0) return null;

        PendedMember pmembers[] = new PendedMember[pms.size()];
        pms.toArray(pmembers);

        return pmembers;
    }

    /**
     *
     * @return Todos os membros que aguardam pela aceitação para ser sócio e já passaram pelo setor de análise.
     * @see #getMembersRequest()
     */
    public @Nullable PendedMember[] getAnalysedMembersRequest(){

        if(!isConnected()){
            System.err.println("Não há conexão com o banco de dados");
            return null;
        }

        PendedMember pm;
        ArrayList<PendedMember> pms = new ArrayList<>(50);
        final String query = " SELECT IDCliente, idade, nome, salario, endereco, email, telefone, parecer FROM Clientes " +
                "WHERE dataAprovacao IS NULL AND motivorecusa IS NULL AND Parecer IS NOT NULL";

        final int columnID= 1;
        final int columnAge = 2;
        final int columnName = 3;
        final int columnSalary = 4;
        final int columnAddress = 5;
        final int columnEmail = 6;
        final int columnPhone = 7;
        final int columnAnalysis = 8;

        try{
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()){
                pm = new PendedMember(rs.getInt(columnID), rs.getInt(columnAge), rs.getString(columnName),
                        rs.getFloat(columnSalary), rs.getString(columnAddress), rs.getString(columnEmail),
                        rs.getString(columnPhone), rs.getString(columnAnalysis));
                fillDependentsFromMember(pm);
                pms.add(pm);
            }

            rs.close();

        }catch(SQLException e){
            e.printStackTrace();
        }


        if(pms.size() == 0) return null;

        PendedMember pmembers[] = new PendedMember[pms.size()];
        pms.toArray(pmembers);

        return pmembers;
    }

    /**
     *
     * @return Todos os sócios.
     */
    public @Nullable AcceptedMember[] getAcceptedMembers(){

        if(!isConnected()){
            System.err.println("Não há conexão com o banco de dados");
            return null;
        }

        ArrayList<AcceptedMember> pms = new ArrayList<>(50);
        final String query = " SELECT IDCliente, idade, nome, salario, endereco, email, telefone, dataaprovacao FROM Clientes " +
                "WHERE dataAprovacao IS NOT NULL";

        final int columnID= 1;
        final int columnAge = 2;
        final int columnName = 3;
        final int columnSalary = 4;
        final int columnAddress = 5;
        final int columnEmail = 6;
        final int columnPhone = 7;
        final int columnApprovalDate = 8;

        AcceptedMember member;

        try{
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()){
                member = new AcceptedMember(rs.getInt(columnID), rs.getInt(columnAge), rs.getString(columnName),
                        rs.getFloat(columnSalary), rs.getString(columnAddress), rs.getString(columnEmail),
                        rs.getString(columnPhone), rs.getDate(columnApprovalDate));

                checkDebts(member);
                fillDependentsFromMember(member);
                pms.add(member);

            }

            rs.close();

        }catch(SQLException e){
            e.printStackTrace();
        }


        if(pms.size() == 0) return null;

        AcceptedMember pmembers[] = new AcceptedMember[pms.size()];
        pms.toArray(pmembers);

        return pmembers;

    }

    /**
     *
     * @return Todos os membros que não foram aceitos como sócios.
     */
    public @Nullable RefusedMember[] getRefusedMembers(){

        if(!isConnected()){
            System.err.println("Não há conexão com o banco de dados");
            return null;
        }

        ArrayList<RefusedMember> pms = new ArrayList<>(50);
        RefusedMember rm;
        final String query = " SELECT IDCliente, idade, nome, salario, endereco, email, telefone, motivorecusa FROM Clientes " +
                "WHERE dataAprovacao IS NULL AND motivorecusa IS NOT NULL";

        final int columnID= 1;
        final int columnAge = 2;
        final int columnName = 3;
        final int columnSalary = 4;
        final int columnAddress = 5;
        final int columnEmail = 6;
        final int columnPhone = 7;
        final int columnRefuseInfo = 8;

        try{
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()){
                rm = new RefusedMember(rs.getInt(columnID), rs.getInt(columnAge), rs.getString(columnName),
                        rs.getFloat(columnSalary), rs.getString(columnAddress), rs.getString(columnEmail),
                        rs.getString(columnPhone), rs.getString(columnRefuseInfo));
                fillDependentsFromMember(rm);
                pms.add(rm);
            }

            rs.close();

        }catch(SQLException e){
            e.printStackTrace();
        }


        if(pms.size() == 0) return null;

        RefusedMember pmembers[] = new RefusedMember[pms.size()];
        pms.toArray(pmembers);

        return pmembers;

    }

    public Attraction[] getAttractions(){

        if(!isConnected()){
            System.err.println("Não há conexão com o banco de dados");
            return null;
        }

        final int attractionIDColumn = 1;
        final int descriptionColumn = 2;

        ArrayList<Attraction> attractions = new ArrayList<>(50);

        final String query = "SELECT idatracao, descricao FROM atracoes ";

        try {

            PreparedStatement ps = connection.prepareStatement(query);

            ResultSet rs = ps.executeQuery();

            while(rs.next()){
                attractions.add(new Attraction(rs.getInt(attractionIDColumn), rs.getString(descriptionColumn)));
            }

            rs.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(attractions.size() == 0) return null;

        Attraction[] a = new Attraction[attractions.size()];
        attractions.toArray(a);

        return a;

    }

    /**
     *
     * @return Número de sócios aceitos, -1 caso não haja conexão com o banco de dados ou -2 caso tenha ocorrido erro na busca
     * dos dados.
     */
    public int getNumAcceptedMembers(){

        if(!isConnected()){
            System.err.println("Não há conexão com o banco de dados");
            return -1;
        }

        int numAcceptedMembers = 0;
        final String query = "SELECT COUNT(*) FROM CLIENTES WHERE DATAAPROVACAO IS NOT NULL AND MOTIVORECUSA IS NULL";

        try{
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            if(!rs.next()) return -2;

            numAcceptedMembers = rs.getInt(1);

        }catch(SQLException e){
            e.printStackTrace();
        }


        return numAcceptedMembers;
    }

    /**
     *
     * @param member Membro que foi analisado
     * @param analysis Parecer sobre o membro
     * @return True se o dado foi inserido com sucesso.
     */
    public boolean storeMemberAnalysis(PendedMember member, String analysis){

        if(!isConnected()){
            System.err.println("Não há conexão com o banco de dados");
            return true;
        }

        final String query = "UPDATE CLIENTES SET parecer = ? WHERE IDCLIENTE = ?";

        final int columnAnalysis = 1;
        final int columnMemberID = 2;

        try{
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(columnAnalysis, analysis);
            ps.setInt(columnMemberID, member.getID());
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
            return false;
        }


        return true;
    }

    public Connection getConnection(){
        return connection;
    }

    /**
     * Verifica todos os débitos de um determinado sócio. Todos os débitos encontrados são passados
     * para a classe via {@link AcceptedMember#setDebts(Debt[])} e {@link AcceptedMember#setPaidDebts(PaidDebt[])}
     * @param member Sócio com ID para consulta.
     */
    private void checkDebts(AcceptedMember member){

        if(!isConnected()){
            System.err.println("Não há conexão com o banco de dados");
            return;
        }

        final int columnDebtID = 1;
        final int columnVal = 2;
        final int columnPayDay = 3;
        final int columnValidity = 4;

        int debtID;
        int val;
        Date payDay;
        Date validity;

        ArrayList<Debt> debts = new ArrayList<>(20);
        ArrayList<PaidDebt> paidDebts = new ArrayList<>(50);

        final String query = "Select IDDivida, valor, datapagamento, datavencimento FROM Divida WHERE IDCliente = ?;";

        try{
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, member.getID());
            ResultSet rs = ps.executeQuery();

            while(rs.next()){
                debtID = rs.getInt(columnDebtID);
                val = rs.getInt(columnVal);
                payDay = rs.getDate(columnPayDay);
                validity = rs.getDate(columnValidity);

                if(payDay == null){
                    debts.add(new Debt(debtID, val, validity));
                }

                else paidDebts.add(new PaidDebt(debtID, val, validity, payDay));

            }

            rs.close();

            if(debts.size() > 0){

                Debt[] d = new Debt[debts.size()];
                debts.toArray(d);
                member.setDebts(d);

            }

            if(paidDebts.size() > 0){

                PaidDebt[] pd = new PaidDebt[paidDebts.size()];
                paidDebts.toArray(pd);
                member.setDebts(pd);

            }

        }catch(SQLException e){
            e.printStackTrace();
        }



    }

    /**
     * Verifica se o sócio possui alguma reserva, caso sim é usada a função
     * {@link AcceptedMember#setReservation(Reservation)} para associar a reserva
     * a classe.
     * @param member Sócio com ID para consulta.
     */
    private void checkMemberReservation(AcceptedMember member){

        if(!isConnected()){
            System.err.println("Não há conexão com o banco de dados");
            return;
        }

        final int columnReservationID = 1;
        final int columnDate = 2;
        final int columnIDAttraction = 3;
        final int columnDescription = 4;

        int reservationID;

        final String query = "SELECT idreserva, data, idatracao, descricao FROM Reserva NATURAL JOIN Atracoes WHERE IDCliente = ?";

        try{
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, member.getID());
            ResultSet rs = ps.executeQuery();

            if(rs.next()){
                reservationID = rs.getInt(columnReservationID);
                member.setReservation(new Reservation(reservationID, rs.getString(columnDate),
                        new Attraction(rs.getInt(columnIDAttraction), rs.getString(columnDescription)),
                        getGuestsFromReservation(reservationID)));
            }

            rs.close();

        }catch(SQLException e){
            e.printStackTrace();
        }

    }

    private Guest[] getGuestsFromReservation(int reservationID){

        ArrayList<Guest> guests = new ArrayList<>(20);

        if(!isConnected()){
            System.err.println("Não há conexão com o banco de dados");
            return null;
        }

        final String query = "SELECT idconvidado, nome FROM convidados WHERE idreserva = ? ";

        final int columnGuestID = 1;
        final int columnsName = 2;

        try{

            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, reservationID);
            ResultSet rs = ps.executeQuery();

            while(rs.next()){
                guests.add(new Guest(rs.getInt(columnGuestID), rs.getString(columnsName)));
            }

            rs.close();

        }catch(SQLException e){
            e.printStackTrace();
        }

        if(guests.size() == 0) return null;

        Guest[] g = new Guest[guests.size()];
        guests.toArray(g);

        return g;
    }

    /**
     * Busca no banco de dados todos os dependentes de um membro em específico e preenche usando {@link Member#addDependent(Dependent)}.
     * @param member Membro que possa conter dependentes
     */
    private void fillDependentsFromMember(Member member){

        if(!isConnected()){
            System.err.println("Não há conexão com o banco de dados");
            return;
        }

        final String query = "SELECT IDDEPENDENTE, NOME FROM DEPENDENTE WHERE IDCLIENTE = ?;";
        final int columnName = 2;
        final int columnID = 1;

        try{
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, member.getID());

            ResultSet rs = ps.executeQuery();

            while(rs.next()){
                System.out.println("ID dependente = " + rs.getInt(columnID) + " para membro id = " + member.getID());
                member.addDependent(new Dependent(rs.getInt(columnID), rs.getString(columnName)));
            }

            rs.close();

        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    public AcceptedMember[] getMembersWithNoDebts(){
        if(!isConnected()){
            System.err.println("Não há conexão com o banco de dados");
            return null;
        }

        ArrayList<AcceptedMember> acceptedMembers = new ArrayList<>(50);

        final String query = "SELECT IDCLIENTE, NOME FROM CLIENTES NATURAL JOIN DIVIDA WHERE DATAPAGAMENTO IS NOT NULL AND " +
                "IDCLIENTE NOT IN (SELECT IDCLIENTE FROM CLIENTES NATURAL JOIN DIVIDA WHERE DATAPAGAMENTO IS NULL)";

        try{
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                acceptedMembers.add(new AcceptedMember(rs.getInt(1), 0, rs.getString(2), 0, null, null, null, null));
            }

        }catch(SQLException e){
            e.printStackTrace();
            return null;
        }

        if(acceptedMembers.size() == 0) return null;

        AcceptedMember[] ams = new AcceptedMember[acceptedMembers.size()];
        acceptedMembers.toArray(ams);
        return ams;
    }

    /**
     *
     * @param d Débito a ser verificado
     * @return True se o débito já venceu
     */
    public boolean checkDebtValidity(Debt d){

        boolean ret = false;

        if(!isConnected()){
            System.err.println("Não há conexão com o banco de dados");
            return false;
        }
        final String query = "SELECT pagamentoVencido(?);";

        try{
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, d.getID());

            ResultSet rs = ps.executeQuery();

            if(rs.next()) ret = rs.getBoolean(1);

            rs.close();

        }catch(SQLException e){
            e.printStackTrace();
        }

        return ret;
    }

    public AcceptedMember[] getInadimplentes(){
        if(!isConnected()){
            System.err.println("Não há conexão com o banco de dados");
            return null;
        }

        final String query = "SELECT DISTINCT idcliente, nome, email, telefone, endereco" +
                " FROM CLIENTES NATURAL JOIN DIVIDA WHERE DIVIDA.DATAVENCIMENTO < NOW() AND DIVIDA.DATAPAGAMENTO IS NULL";

        ArrayList<AcceptedMember> members = new ArrayList<>(50);

        try{
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();

            while(rs.next()){
                members.add(new AcceptedMember(rs.getInt(1), 0, rs.getString(2), 0,
                        rs.getString(5), rs.getString(3), rs.getString(4), null));
            }

            rs.close();
        }catch(SQLException e){
            e.printStackTrace();
            return null;
        }

        if(members.size() == 0 ) return null;

        AcceptedMember[] m = new AcceptedMember[members.size()];
        members.toArray(m);

        return m;
    }

    public boolean storeContactAttempt(Member m, String info){

        if(!isConnected()){
            System.err.println("Não há conexão com o banco de dados");
            return false;
        }

        final String query = "INSERT INTO TENTATIVACONTATO (IDCLIENTE, INFO) VALUES(?, ?) ";

        try{
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, m.getID());
            ps.setString(2, info);
            ps.executeUpdate();

        }catch(SQLException e){
            e.printStackTrace();
            return false;
        }

        return true;

    }

    public boolean confirmPayment(AcceptedMember member){
        if(!isConnected()){
            System.err.println("Não há conexão com o banco de dados");
            return false;
        }

        String query = "UPDATE DIVIDA SET DATAPAGAMENTO = NOW() WHERE IDCLIENTE = ? AND DATAPAGAMENTO IS NULL";

        try{
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, member.getID());
            ps.executeUpdate();

        }catch (SQLException e){
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public boolean calculatePayment(Member m){
        if(!isConnected()){
            System.err.println("Não há conexão com o banco de dados");
            return false;
        }

        final String query = "INSERT INTO DIVIDA (VALOR, DATAPAGAMENTO, DATAVENCIMENTO, IDCLIENTE) " +
                " VALUES(?, NULL, '2016-07-29', ?)";
        float valor = getJoia() + 50f ;

        try{
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setFloat(1, valor);
            ps.setInt(2, m.getID());
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
            return false;
        }

        return true;
    }

    private float getJoia(){

        final String query = "SELECT SUM(*) * 0.2 FROM DIVIDAS WHERE DATAPAGAMENTO IS NOT NULL";

        try{
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();

            if(rs.next()) return rs.getFloat(1);
        }catch(SQLException e){
            e.printStackTrace();
            return 0f;
        }

        return 0f;
    }


}
