package com.mod2.project.db.entity.member;

import com.mod2.project.db.entity.member.Member;

/**
 * Membros que não foram aceitos
 */
public class RefusedMember extends Member {

    private String refuseInfo;

    public RefusedMember(int id, int age, String name, float salary, String address, String email, String phone, String refuseInfo) {
        super(id, age, name, salary, address, email, phone, null);
        this.refuseInfo = refuseInfo;
    }

    /**
     *
     * @return Motivo de esta pessoa ter sido recusada como sócio.
     */
    public String getRefuseInfo(){ return refuseInfo; }



}
