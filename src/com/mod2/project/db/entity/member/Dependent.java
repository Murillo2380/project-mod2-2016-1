package com.mod2.project.db.entity.member;

import com.mod2.project.db.entity.Person;

/**
 * Dependentes de clientes
 */
public class Dependent extends Person {

    public Dependent(int id, String name) {
        super(id, name);
    }

}
