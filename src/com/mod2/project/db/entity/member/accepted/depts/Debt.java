package com.mod2.project.db.entity.member.accepted.depts;

import com.mod2.project.db.entity.DBEntity;

import java.util.Date;

/**
 * Classe que contém dados de pagamento pendente.
 */
public class Debt extends DBEntity {

    private float value;

    private Date validity;

    public Debt(int id, float value, Date validity) {
        super(id);
        this.validity = validity;
        this.value = value;

    }

    /**
     *
     * @return Valor do débito.
     */
    public float getValue(){ return value; }

    /**
     *
     * @return Validade do débito.
     */
    public Date getValidity(){ return validity; }

}
