package com.mod2.project.db.entity.member.accepted;

import com.mod2.project.db.entity.member.Member;
import com.mod2.project.db.entity.member.accepted.depts.Debt;
import com.mod2.project.db.entity.member.accepted.depts.PaidDebt;
import com.mod2.project.db.entity.member.accepted.reservation.Reservation;

import java.util.Date;

/**
 * Classe que contém atributos de um membro aceito como sócio.
 */
public class AcceptedMember extends Member {

    private Date approvalDate;
    private Debt[] debts;
    private PaidDebt[] paidDebts;
    private Reservation reservation;

    public AcceptedMember(int id, int age, String name, float salary, String address, String email, String phone, Date approvalDate) {
        super(id, age, name, salary, address, email, phone, null);
        this.approvalDate = approvalDate;
        debts = null;
        paidDebts = null;
    }

    /**
     *
     * @param debts Débitos que este sócio possui.
     */
    public void setDebts(Debt[] debts){
        this.debts = debts;
    }

    /**
     *
     * @param paidDebts Débitos que já foram pagos por este sócio.
     */
    public void setPaidDebts(PaidDebt[] paidDebts){
        this.paidDebts = paidDebts;
    }

    /**
     *
     * @param reservation Reserva que este sócio possa ter feito.
     */
    public void setReservation(Reservation reservation){
        this.reservation = reservation;
    }

    /**
     *
     * @return Informações sobre a reserva feita por este sócio, null se não houver.
     */
    public Reservation getReservation(){
        return reservation;
    }

    /**
     *
     * @return Débitos que este sócio ainda possui.
     */
    public Debt[] getDebts(){
        return debts;
    }

    /**
     *
     * @return Débitos que este sócio já pagou.
     */
    public PaidDebt[] getPaidDebts(){
        return paidDebts;
    }

    /**
     *
     * @return Dia que esta pessoa foi aceita como sócio.
     */
    public Date getApprovalDate(){ return approvalDate; }

    /**
     *
     * @return Total a ser pago por este sócio
     */
    public float getTotalDebtValue(){

        if(debts == null) return 0;

        float total = 0;

        for(Debt d : debts)
            total += d.getValue();

        return total;

    }

}
