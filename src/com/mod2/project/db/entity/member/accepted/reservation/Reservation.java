package com.mod2.project.db.entity.member.accepted.reservation;

import com.mod2.project.db.entity.DBEntity;

/**
 * Classe que contém informações sobre uma reserva feita por um sócio em específico
 */
public class Reservation extends DBEntity {

    private Attraction attraction;
    private Guest[] guests;
    private String reservationDate;

    public Reservation(int id, String reservationDate, Attraction attraction, Guest[] guests) {
        super(id);
        this.reservationDate = reservationDate;
        this.attraction = attraction;
        this.guests = guests;
    }

    /**
     *
     * @return Lista de convidados
     */
    public Guest[] getGuestList(){
        return guests;
    }

    /**
     *
     * @return Atração reservada
     */
    public Attraction getReservedAttraction(){
        return attraction;
    }

    public String getReservationDate(){ return reservationDate; }

}
