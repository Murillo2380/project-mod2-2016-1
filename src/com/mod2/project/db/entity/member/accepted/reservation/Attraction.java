package com.mod2.project.db.entity.member.accepted.reservation;

import com.mod2.project.db.entity.DBEntity;

/**
 * Atrações como piscina, quadra, etc.
 */
public class Attraction extends DBEntity {

    private String description;

    public Attraction(int id, String description){
        super(id);
        this.description = description;
    }

    /**
     *
     * @return Descrição a atração
     */
    public String getDescription(){ return description; }

}
