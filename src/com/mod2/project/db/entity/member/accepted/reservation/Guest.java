package com.mod2.project.db.entity.member.accepted.reservation;

import com.mod2.project.db.entity.Person;

/**
 * Classe de convidados de uma reserva.
 */
public class Guest extends Person {

    public Guest(int id, String name) {
        super(id, name);
    }

}
