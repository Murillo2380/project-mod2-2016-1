package com.mod2.project.db.entity.member.accepted.depts;

import com.mod2.project.db.entity.member.accepted.depts.Debt;

import java.util.Date;

/**
 * Classe de débitos pagos.
 */
public class PaidDebt extends Debt {

    private Date payDay;

    public PaidDebt(int id, float value, Date validation, Date payDay) {
        super(id, value, validation);
    }

    /**
     *
     * @return Dia em que este débito foi pago.
     */
    private Date getPayDay(){
        return payDay;
    }

}
