package com.mod2.project.db.entity.member;

import com.mod2.project.db.entity.member.Member;

/**
 * Classe para membros pendendes, que precisam da aprovação para se tornarem sócios.
 */
public class PendedMember extends Member {

    public PendedMember(int age, String name, float salary, String address, String email, String phone, String analysis) {
        super(age, name, salary, address, email, phone, analysis);
    }

    public PendedMember(int id, int age, String name, float salary, String address, String email, String phone, String analysis) {
        super(id, age, name, salary, address, email, phone, analysis);
    }

}
