package com.mod2.project.db.entity.member;

import com.mod2.project.db.entity.DBEntity;
import com.mod2.project.db.entity.Person;
import com.mod2.project.db.entity.member.accepted.depts.Debt;

/**
 * Classe com os dados pertinentes aos clientes.
 */
public class Member extends Person {

    private static final int MAX_DEPENDENTS = 5;

    private Dependent[] dependents;
    private int numDependents;
    private int age;

    private float salary;

    private String email;
    private String phone;
    private String address;
    private String analysis;

    public Member(int age, String name, float salary, String address, String email, String phone, String analysis) {
        super(DBEntity.INVALID_ID, name);
        this.age = age;
        this.salary = salary;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.analysis = analysis;

        dependents = new Dependent[MAX_DEPENDENTS];
        numDependents = 0;
    }

    public Member(int id, int age, String name, float salary, String address, String email, String phone, String analysis) {
        super(id, name);
        this.age = age;
        this.salary = salary;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.analysis = analysis;

        dependents = new Dependent[5];
        numDependents = 0;
    }

    public float getSalary(){ return salary; }

    public int getAge(){ return age; }

    public String getEmail(){ return email; }

    public String getPhone(){ return phone; }

    public String getAddress(){ return address; }

    public String getAnalysis(){ return analysis; }

    public Dependent[] getDependents(){
        Dependent[] d = new Dependent[numDependents];
        System.arraycopy(d, 0, dependents, 0, numDependents);
        return d;
    }

    public int getNumDependents(){ return numDependents; }

    /**
     *
     * @param d Dependente a ser adicionado para este cliente.
     * @return True se foi adicionado, false caso não tenha sido adicionado por ultrapassar o limite de dependentes permitidos
     */
    public boolean addDependent(Dependent d){

        if(numDependents == MAX_DEPENDENTS) return false;
        dependents[numDependents++] = d;
        return true;

    }

}
