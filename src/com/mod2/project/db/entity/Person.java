package com.mod2.project.db.entity;

/**
 * Classe que define atributos básicos de uma pessoa para esta aplicação.
 */
public class Person extends DBEntity {

    private String name;

    public Person(int id, String name){
        super(id);
        this.name = name;
    }

    public String getName(){ return name; }

}
