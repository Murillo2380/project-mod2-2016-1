package com.mod2.project.db.entity;

/**
 * Representa uma entidade do banco de dados deste projeto.
 */
public abstract class DBEntity {

    /**
     * Geralmente usado o objeto não foi criado a partir do banco
     */
    public static final int INVALID_ID = -1;

    private int id;

    public DBEntity(int id){
        this.id = id;
    }

    public int getID(){
        return id;
    }

}
